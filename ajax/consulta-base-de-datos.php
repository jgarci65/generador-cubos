<?php
include_once '../config/path.php';

if(isset($_GET) and $_GET['IdBaseDatos'] != ""){
    
    include_once RUTA_REPORTEADOR.'clases/generador/Configurador.php';
    $Configurador = new Configurador(CONECTAR_A);
    $Limpio = $_GET;
    
    $BaseDatos = $Configurador->getBaseDeDatos()->consulta($Limpio['IdBaseDatos']);

    if (count($BaseDatos) > 0) {
        echo json_encode($BaseDatos[0]);
    }
}