<?php
include_once '../config/path.php';

if(isset($_GET) and $_GET['Informe'] != ""){
    
    include_once RUTA_REPORTEADOR.'clases/generador/Configurador.php';
    $Configurador = new Configurador(CONECTAR_A);
    $Limpio = $_GET;
    echo json_encode($Configurador->getTablaRecurrente()->consultaPorNiveles($Limpio['Informe'], $Limpio['Cliente']));
}