<?php
include_once '../../config/path.php';

if(isset($_GET) and isset($_GET['Cliente']) and isset($_GET['Informe']) and isset($_GET['SiguienteNivel'])){
    
    include_once '../../clases/generador/Generador.php';
    $Generador = new Generador();
    $Informe = $Generador->generarInforme(
        $_GET['Cliente'], 
        $_GET['Informe'], 
        $_GET['SiguienteNivel'], 
        (isset($_GET['Tabla']) ? $_GET['Tabla'] : null),
        $_GET
    );
    if(count($Informe) > 0) echo json_encode($Informe);
}