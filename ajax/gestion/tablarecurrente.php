<?php
include_once '../../config/path.php';

if (isset ($_POST) and count($_POST) > 0) {
    
    include_once RUTA_REPORTEADOR.'clases/generador/Configurador.php';
    $Configurador = new Configurador(CONECTAR_A);
    $Limpio = $_POST;
    
    if($Limpio['Hacer'] == 'consultar'){
        
        $Exitoso = false;
        $Mensaje = "";
        
        $Tablas = $Configurador->consultaTablaRecurrente($Limpio);
        if(count($Tablas) > 0){
            $Exitoso = true;
        }
        else{
            $Tablas[0] = $Configurador->insertaTablaRecurrente($Limpio);
            $Exitoso = true;
        }
        
        echo json_encode(
            array(
                'Tablas' => $Tablas,
                'Exitoso' => $Exitoso,
                'Mensaje' => $Mensaje
            )
        );
    }
    elseif ($Limpio['Hacer'] == 'guardar') {
        
        $Limpio['Sql'] = $_POST['Sql'];
        $Cambios = $Configurador->diferenciaTablaRecurrente($Limpio);
        echo json_encode(
            array(
                'Cambios' => $Cambios
            )
        );
    }
    elseif ($Limpio['Hacer'] == 'eliminar') {

        echo $Configurador->eliminarTablaRecurrente($Limpio['IdTabla'], $Limpio['Cliente'], $Limpio['Informe']);
    }
    elseif ($Limpio['Hacer'] == 'insertar') {

        echo json_encode($Tablas = $Configurador->insertaTablaRecurrente($Limpio));
    }
    elseif ($Limpio['Hacer'] == 'duplicar' and $Limpio['Duplicando'] == 'informe') {

        echo json_encode($Configurador->duplicarInforme($Limpio));
    }
    elseif ($Limpio['Hacer'] == 'duplicar' and $Limpio['Duplicando'] == 'tabla' and (isset($Limpio['IdTablaPadrePegar']) || isset($Limpio['InformePegar']))){
        
        $Configurador->duplicarTabla(
            NULL, 
            NULL, 
            NULL, 
            $Limpio['IdTablaCopiar'], 
            $Limpio['InformePegar'], 
            (isset($Limpio['IdTablaPadrePegar']) ? $Limpio['IdTablaPadrePegar'] : NULL), 
            $Limpio['CopiarSubNivel']
        );
        echo json_encode(
            array(
                'Duplicadas' => $Configurador->getDuplicadas(),
            )
        );
    }
}