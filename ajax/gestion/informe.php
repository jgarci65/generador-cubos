<?php
include_once '../../config/path.php';

if(isset($_POST) and $_POST['NombreInforme'] != "" and $_POST['ClienteEnCR'] != "" and $_POST['Hacer'] != ""){
    
    include_once RUTA_REPORTEADOR.'clases/generador/Configurador.php';
    $Configurador = new Configurador(CONECTAR_A);
    $Limpio = $_POST;
    
    echo json_encode($Configurador->gestionTablaInforme($Limpio));
}