<?php
include_once '../config/path.php';

if(isset($_GET) and $_GET['Cliente'] != ""){
    
    include_once RUTA_REPORTEADOR.'clases/generador/Configurador.php';
    $Configurador = new Configurador(CONECTAR_A);
    
    echo json_encode(
        $Configurador->crearArrayParaSelect(
            $Configurador->getInforme()->consulta($_GET['Cliente']), 
            'id_informe',
            'nombre_informe'
        )
    );
}