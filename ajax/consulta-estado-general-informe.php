<?php
include_once '../config/path.php';

if(isset($_GET) and count($_GET) > 0 and $_GET['Informe'] > 0 and $_GET['Cliente'] > 0){
    
    include_once RUTA_REPORTEADOR.'clases/generador/Configurador.php';
    $Configurador = new Configurador(CONECTAR_A);
    $Limpio = $_GET;
    
    echo json_encode(
        array(
            'Errores' => $Configurador->estadoGeneralInforme($Limpio['Informe'], $Limpio['Cliente'])
        )
    );
}
