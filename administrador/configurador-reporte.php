<?php
include_once '../config/path.php';

# Controlador, inicio de sesión
include_once '../clases/View.php';
$Vista = new View();

include_once '../clases/generador/Configurador.php';
$Configurador = new Configurador(CONECTAR_A);

# Datos a reamplazar en el template de la vista
$RemplazaAVista = array(
    'ListadoClientes' => json_encode(
        $Configurador->crearArrayParaSelect(
            $Configurador->getCliente()->consuta(), 
            'id_cliente',
            'nombre'
        )
    ),
    'ListadoBaseDatos' => json_encode(
        $Configurador->crearArrayParaSelect(
            $Configurador->getBaseDeDatos()->consulta(), 
            'id_basededatos', 
            'nombre'
        )
    ),
    'ListadoEventos' => json_encode(
        $Configurador->crearArrayParaSelect(
            $Configurador->getEventoJavascript()->consulta(),
            'id_evento_javascript', 
            'nombre'
        )
    ),
);

$ConfiguracionVW = array(
    "TITLE" => 'Configurador de reportes',
    "CSS" => array(
        "TEMA" => '<link href="'.$Vista->get_url().'css/navbar-azul.css" rel="stylesheet">',
        "DATA-TABLES-BOOTSTRAP" => '<link href="'.$Vista->get_url().'css/dataTables.bootstrap.min.css" rel="stylesheet">',
        "DATA-TABLES" => '<link href="'.$Vista->get_url().'css/jquery.dataTables.min.css" rel="stylesheet">',
    ),
    "JAVASCRIPT-FOOTER" => array(
        'DATA-TABLES' => '<script type="text/javascript" src="'.$Vista->get_url().'js/jquery.dataTables.min.js"></script>',
        'DATA-TABLES-BOOTSTRAP' => '<script type="text/javascript" src="'.$Vista->get_url().'js/dataTables.bootstrap.min.js"></script>',
        'AUTO-SIZE' => '<script type="text/javascript" src="'.$Vista->get_url().'js/autosize.min.js"></script>',
        'CREA-SELECT' => '<script type="text/javascript" src="'.$Vista->get_url().'js/jquery.creaSelect.js"></script>',
        'JQUERY-CONFIGURADOR-REPORTES' => '<script type="text/javascript" src="'.$Vista->get_url().'js/jquery.configuradorReportes.js"></script>',
        'REPLACE-ALL-JS' => '<script type="text/javascript" src="'.$Vista->get_url().'js/replaceAll.js"></script>',
        'GESTION-CLIENTE' => '<script type="text/javascript" src="'.$Vista->get_url().'js/gestion/cliente.js"></script>',
        'GESTION-INFORME' => '<script type="text/javascript" src="'.$Vista->get_url().'js/gestion/informe.js"></script>',
        'GESTION-TABLARECURRENTE' => '<script type="text/javascript" src="'.$Vista->get_url().'js/gestion/tablarecurrente.js"></script>',
        'GESTION-BASEDEDATOS' => '<script type="text/javascript" src="'.$Vista->get_url().'js/gestion/basededatos.js"></script>',
    ),
    "CONTENT" => $Vista->set_contenido_vista($RemplazaAVista, "view/generador/configurador-reporte.html")
);
echo $Vista->get_layout($ConfiguracionVW);