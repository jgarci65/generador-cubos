<?php
include_once '../config/path.php';

# Controlador, inicio de sesion
include_once '../clases/View.php';
$Vista = new View();

if(isset($_GET) and count($_GET) > 0 and isset($_GET['IdInforme'])){
    
    include_once RUTA_REPORTEADOR.'clases/generador/Configurador.php';
    $Configurador = new Configurador(CONECTAR_A);

    $TablasNivel1 = $Configurador->getTablaRecurrente()->consulta(NULL, 1, NULL, $_GET['IdInforme'], NULL);

    $Sqls = array();
    foreach ($TablasNivel1 AS $TN1){
        $Sqls[] = $TN1['sql'];
    }

    # Datos a reamplazar en el template de la vista
    $RemplazaAVista = array(
        'DatosInforme' => json_encode($Configurador->getInforme()->consulta(NULL, NULL, $_GET['IdInforme'])),
        'Variables' => json_encode($Configurador->getTablaRecurrente()->consultaVariablesSql(implode(";", $Sqls))),
        'RutaReporteador' => CARPETA_REPORTEADOR
    );

    $ConfiguracionVW = array(
        "TITLE" => 'Prueba de reportes',
        "CSS" => array(
            "TEMA" => '<link href="'.$Vista->get_url().'css/navbar-azul.css" rel="stylesheet">',
            "DATA-TABLES-BOOTSTRAP" => '<link href="'.$Vista->get_url().'css/dataTables.bootstrap.min.css" rel="stylesheet">',
            "DATA-TABLES" => '<link href="'.$Vista->get_url().'css/jquery.dataTables.min.css" rel="stylesheet">',
        ),
        "JAVASCRIPT-FOOTER" => array(
            'DATA-TABLES' => '<script type="text/javascript" src="'.$Vista->get_url().'js/jquery.dataTables.min.js"></script>',
            'DATA-TABLES-BOOTSTRAP' => '<script type="text/javascript" src="'.$Vista->get_url().'js/dataTables.bootstrap.min.js"></script>',
            'PROBAR-REPORTES' => '<script type="text/javascript" src="'.$Vista->get_url().'js/probar-reporte.js"></script>',
            'GENERADOR-REPORTES' => '<script type="text/javascript" src="'.$Vista->get_url().'js/jquery.generadorReportes.js"></script>',
        ),
        "CONTENT" => $Vista->set_contenido_vista($RemplazaAVista, "view/generador/probar-reporte.html")
    );
    echo $Vista->get_layout($ConfiguracionVW);
}