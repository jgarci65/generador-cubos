(function($){

  $.fn.generadorReporte = function(Opciones){

    var Configuracion = {
      AleatorioTabla: Math.random().toString(36).substring(7),
      Tabla: new Array(),
      Campos: new Array(),
      IdTabla: null,
      IdTablaPadre: null,
      TablaHija: 0,
      Nivel: null,
      OpcionesTabla: {
        "head": true,
        "foot": true,
        "caption": true
      },
      OpcionesDataTable: {
        "paging": false,
        "ordering": true,
        "searching": true,
        "language": {
            "url": "../js/Spanish.json"
        },
        "columnDefs": [ {
            "targets"  : 'no-sort',
            "orderable": false,
        }]
      }
    };
    $.extend(Configuracion, Opciones);

    function _fnCrearInputHidden(Datos, Fila){
      var SiguienteNivel = parseInt(Configuracion.Nivel) + 1;
      var HtmlInputs = '<input type="hidden" name="Tabla" value="'+Configuracion.IdTabla+'" />'
        + '<input type="hidden" name="SiguienteNivel" value="'+SiguienteNivel+'" />';
      $.each(Datos, function(idx, el) {
        HtmlInputs += '<input type="hidden" name="'+idx+'" id="'+idx+Fila+'" value="'+el+'" />';
      });
      return HtmlInputs;
    }

    function _fnCrearTd(Datos, Campos, Eventos, IdTabla, Fila){
      var HtmlTd = "";
      $.each(Campos, function(idxoc, OC) {
        if(Eventos[idxoc] != undefined){
          HtmlTd += '<td><div style="cursor:pointer; color:#0098e3;" title="'+Eventos[idxoc].descripcion+'" '+Eventos[idxoc].tipo+"=\"fnEventoDinamico('"+Eventos[idxoc].evento+"','frmtb"+IdTabla+"F"+Fila+"')\"><span class='glyphicon "+Eventos[idxoc].icono+"'> </span> "+Datos[idxoc]+"</div></td>";
        }
        else{
          HtmlTd += "<td>"+Datos[idxoc]+"</td>";
        }
      });
      return HtmlTd;
    }

    function _fnCrearTHead(Campos){
      var HtmlHead = "<thead>"
        + "<tr>"
        + "<th "+((parseInt(Configuracion.TablaHija) == 0) ? " style='display: none;'" : "")+" class='no-sort'></th>";
      $.each(Campos, function(index, el) {
        HtmlHead += "<th>"+el+"</th>"
      });
      HtmlHead += "</tr>"
      + "</thead>";
      return HtmlHead;
    }

    function _fnCrearTBody(Tabla, Campos, Eventos){
      var HtmlBody = "<tbody>", Fila = 0;
      if(Tabla.length > 0){
        $.each(Tabla, function(index, Datos) {
          Fila++;
          HtmlBody += "<tr>"
              + "<td "+((parseInt(Configuracion.TablaHija) == 0) ? "style='display: none;'" : "")+">"
                + '<form id="frmtb'+Configuracion.AleatorioTabla+'F'+Fila+'"><span class="glyphicon glyphicon-plus abrirSubNivel"> </span>'
                + _fnCrearInputHidden(Datos, Fila)+"</form>"
                + "</td>"
                + _fnCrearTd(Datos, Campos, Eventos, Configuracion.AleatorioTabla, Fila)
              + "</tr>";
        });
      }
      HtmlBody += "</tbody>";
      return HtmlBody;
    }

    function _fnCrearTFoot(Campos){
      var HtmlFoot = "<tfoot>"
        + "<tr>"
        + ""+(parseInt(Configuracion.TablaHija == 0) ? "<th></th>" : "")+"";
      $.each(Campos, function(index, el) {
        HtmlFoot += "<th>"+el+"</th>"
      });
      HtmlFoot += "</tfoot>";
      return HtmlFoot;
    }

    this.each(function() {
      if($(Configuracion.Campos).size() == 0){
        alert("La tabla ("+Configuracion.Descripcion+") no tiene configurados los campos a mostrar, contacte al administrador del sistema.");
      }
      else{
        Html = _fnCrearTHead(Configuracion.Campos);
        Html += _fnCrearTBody(Configuracion.Tabla, Configuracion.Campos, Configuracion.Eventos);
        if(Configuracion.OpcionesTabla.foot == true){
          Html += _fnCrearTFoot(Configuracion.Campos);
        }
        $(this).append("<div style='"+((Configuracion.Nivel > 1) ? "background-color: "+fnColorFondo()+";" : "")+"; padding: 4px; border-radius: 8px; margin-top: 10px;'>"
          + "<h3>"+Configuracion.Descripcion+"</h3>"
          + "<table id='tb"+Configuracion.AleatorioTabla+"' width='100%' class='table table-border table-hover table-responsive'>"
            + Html
          + "</table>"
        + "<div>");
        $("#tb"+Configuracion.AleatorioTabla).DataTable(Configuracion.OpcionesDataTable);
      }
    });
  };

  $("#frmNivel0").on('submit', function(event) {
    event.preventDefault();

    $.ajax({
      url: $("#frmNivel0").attr("action"),
      type: 'GET',
      dataType: 'json',
      data: $("#frmNivel0").serializeArray(),
      cache: false
    })
    .done(function(json) {
      $("#Nivel1").html("");
      $.each(json, function(idx, Informe) {
        $("#Nivel1").generadorReporte(Informe);
      });
    })
    .fail(function() {
      alert("Ocurrió un problema, actualice la página y vuelva a intentarlo, en caso de persistir, contacte al administrador del sistema.");
    });
  });

  $("#Nivel1").on('click', '.abrirSubNivel', function(event) {
    var Estoy = this;
    if ($(Estoy).hasClass('glyphicon-plus')){
      event.preventDefault();
      event.stopPropagation();
      var frmActual = this.parentElement.getAttribute("id");
      var DatosForm = $("#"+frmActual).serialize();
      DatosForm += "&Cliente="+$("#frmNivel0 > .Cliente").val()+"&Informe="+$("#frmNivel0 > .Informe").val();
      $.ajax({
        url: $("#frmNivel0").attr("action"),
        type: 'GET',
        dataType: 'json',
        data: DatosForm
      })
      .done(function(json) {
        var trSubNivel = "subTr"+frmActual;
        var Columnas = $("#"+$("#"+frmActual).parents("table").attr("id")+" > thead > tr > th").size();
        $("#"+frmActual).parent().parent().after("<tr><td id='"+trSubNivel+"' colspan='"+Columnas+"'></td></tr>");
        $(Estoy).removeClass("glyphicon-plus").addClass('glyphicon-minus');
        $.each(json, function(idx, Informe) {
          $("#"+trSubNivel).generadorReporte(Informe);
        });
      })
      .fail(function() {
        alert("Ocurrio un problema, valide al configuracion del reporte");
      });
    }
    else{
      $(Estoy).removeClass("glyphicon-minus").addClass('glyphicon-plus');
      $(Estoy).parent().parent().parent().next().remove();
    }
    return false;
  });
})(jQuery);

function fnEventoDinamico(Evento, DondeEstoy){
  if(typeof window[Evento] === 'function') {
    Datos = fnPasaDatosFila(DondeEstoy);
    window[Evento](Datos);
  }
  else{
    alert("No existe la funcion ("+Evento+") contacte al administrador del sistema.");
  }
}

function fnPasaDatosFila(DondeEstoy){
  var Datos = $.parseJSON(JSON.stringify($('#'+DondeEstoy).serializeArray()));
  jsonDatos = new Object();
  $.each(Datos, function(idx, Contenido) {
    jsonDatos[Contenido['name']] = Contenido['value'];
  });
  return jsonDatos;
}

function fnColorFondo(){

  var Colores = new Array("#D8D8D8", "#F2F2F2", "#E6E6E6");
  return Colores[Math.round( Math.random() * (Colores.length - 1))];
}
