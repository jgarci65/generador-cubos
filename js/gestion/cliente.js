$(function(){

  $("#Cliente").creaSelect(jsonClientes);

  $("#btnCreaCliente, #btnEditaCliente").on('click', function(event) {
    event.preventDefault();
    document.getElementById("frmGestionCliente").reset();

    var Seguir = false;
    if ($(this).attr('id') == 'btnCreaCliente') {
      Hacer = 'crear';
      $("#IdClienteGC").val('');
      Seguir = true;
    }
    else if ($(this).attr('id') == 'btnEditaCliente') {
      Hacer = 'editar';
      $("#IdClienteGC").val($("#Cliente").val());
      $("#NombreCliente").val($("#Cliente option:selected").html());
      Seguir = true;
    }
    if(Seguir == true){
      $("#HacerCliente").val(Hacer);
      $("#btnSubmitCliente").html(Hacer.charAt(0).toUpperCase() + Hacer.slice(1));
      $("#m-gestion-cliente").modal("show");
    }
    else{
      alert("Debe seleccionar el cliente a "+Hacer);
    }
  });

  $("#frmGestionCliente").on('submit', function(event) {
    event.preventDefault();

    $.ajax({
      url: '../ajax/gestion/cliente.php',
      type: 'POST',
      dataType: 'json',
      data: $("#frmGestionCliente").serialize()
    })
    .done(function(rjson) {
      if(rjson.Exitoso == true){
        $("#Cliente").creaSelect(rjson.JsonClientes);
        $("#Cliente").val(rjson.IdCliente);
        alert(rjson.Mensaje);
        $("#m-gestion-cliente").modal("hide");
        $("#btnAgregarTabla").css('display', 'none');
        $("#Cliente").change();
      }
      else{
        alert("No se pudo realizar el proceso: "+rjson.Mensaje);
      }
    })
    .fail(function(error) {
      $("#MensajeError").html(error.responseText);
      $("#m-error").modal("show");
    });
  });

  $("#Cliente").on('change', function(event) {
    event.preventDefault();

    var IdCliente = $(this).val();
    if(IdCliente != ""){
      informesCliente(IdCliente, 'Informe');
    }
    else{
      $("#Informe").html('');
    }
    $(".EspacioInforme").html("");
    $("#btnAgregarTabla").css('display', 'none');
  });
});

function informesCliente(Cliente, SelectCrear){

  $.ajax({
    url: '../ajax/consulta-informe.php',
    type: 'GET',
    dataType: 'json',
    data: {
      'Cliente': Cliente
    }
  })
  .done(function(rjson) {
    $("#"+SelectCrear).creaSelect(rjson);
  })
  .fail(function(error) {
    $("#MensajeError").html(error.responseText);
    $("#m-error").modal("show");
  });
}
