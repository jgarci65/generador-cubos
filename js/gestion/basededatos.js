$(function(){

  $("#IdBaseDatos").creaSelect(jsonBaseDatos);

  $(".EspacioInforme").on('click', ".btnCreaBaseDatos, .btnEditaBaseDatos, .btnEliminaBaseDatos", function(event) {
    event.preventDefault();
    document.getElementById("frmGestionBaseDatos").reset();

    var Seguir = false;
    var IdTabla = $(this).parents("form").children('.IdTabla').val();
    $("#frmGestionBaseDatos").children('.IdTabla').val(IdTabla);

    if($(this).hasClass('btnCreaBaseDatos') == true){
      Hacer = 'crear';
      $("#IdBaseDatosGBD").val('');
      Seguir = true;
    }
    else if ($(this).hasClass('btnEditaBaseDatos') == true && $("#IdBaseDatos").val() != '') {
      Hacer = 'editar';
      $("#Nombre").val($("#IdBaseDatos"+IdTabla+" option:selected").html());
      $("#IdBaseDatosGBD").val($("#IdBaseDatos"+IdTabla).val());
      Seguir = true;

    }
    else if ($(this).hasClass('btnEliminaBaseDatos') == true && $("#IdBaseDatos").val() != '') {
      Hacer = 'eliminar';
      $("#NombreBaseDatos").val($("#IdBaseDatos"+IdTabla+" option:selected").html());
      $("#IdBaseDatosGBD").val($("#IdBaseDatos"+IdTabla).val());
      Seguir = true;
    }

    if(Seguir == true){
      $("#HacerBaseDatos").val(Hacer);
      $("#btnSubmitBaseDatos").html(Hacer.charAt(0).toUpperCase() + Hacer.slice(1));
      if(Hacer != 'crear'){

        $.ajax({
          url: '../ajax/consulta-base-de-datos.php',
          type: 'GET',
          dataType: 'json',
          data: $("#frmGestionBaseDatos").serialize()
        })
        .done(function(rjson) {
          $("#Nombre").val(rjson.nombre);
          $("#Host").val(rjson.host);
          $("#Usuario").val(rjson.usuario);
          $("#Clave").val(rjson.clave);
          $("#DB").val(rjson.base_de_datos);
          $("#Esquema").val(rjson.esquema);
          $("#Puerto").val(rjson.puerto);
        })
        .fail(function(error) {
          $("#MensajeError").html(error.responseText);
          $("#m-error").modal("show");
        });

      }
      $("#m-gestion-basedatos").modal("show");
    }
    else{
      alert("Debe seleccionar la base de datos a gestionar");
    }

  });

  $("#frmGestionBaseDatos").on('submit', function(event) {
    event.preventDefault();

    var IdTabla = $(this).children('.IdTabla').val();
    $.ajax({
      url: '../ajax/gestion/basededatos.php',
      type: 'POST',
      dataType: 'json',
      data: $("#frmGestionBaseDatos").serialize()
    })
    .done(function(rjson) {
      if(rjson.Exitoso == true){
        $("#IdBaseDatos"+IdTabla).creaSelect(rjson.JsonBaseDatos);
        if(rjson.Hacer != 'eliminar'){
          $("#IdBaseDatos"+IdTabla).val(rjson.IdBaseDatos);
        }
        alert(rjson.Mensaje);
        $("#m-gestion-basedatos").modal("hide");
      }
      else{
        alert("No se pudo realizar el proceso: "+rjson.Mensaje);
      }
    })
    .fail(function(error) {
      $("#MensajeError").html(error.responseText);
      $("#m-error").modal("show");
    });
  });

  $(".EspacioInforme").on('blur', '.Sql', function(event) {
    event.preventDefault();

    var Sql = $(this).val();
    validarPermitidas(Sql);
  });

  $(".EspacioInforme").on('click', '.btnProbarQuery', function(event) {
    event.preventDefault();

    var IdTabla = $(this).parents("form").children('.IdTabla').val();
    var Sql = $("#Sql"+IdTabla).val();
    var Permitida = validarPermitidas(Sql);
    
    if(Sql != "" && $("#IdBaseDatos"+IdTabla).val() != "" && Permitida == true){

      var Variables = obtenerVariablesQuery(Sql);
      var Html = '<input type="hidden" name="IdBaseDatos" value="'+$("#IdBaseDatos"+IdTabla).val()+'">'
      + '<input type="hidden" name="Sql" class="Sql" value="'+Sql+'">'
      + '<pre id="VistaSql">'+Sql+'</pre>'
      $.each(Variables, function(idx, Va) {
        Html += '<div class="form-group">'
          + '<label for="'+Va+'" class="col-md-3">'+Va+'</label>'
          + '<div class="col-md-9">'
            + '<input type="text" class="form-control CampoPrueba" name="'+Va+'" placeholder="">'
          + '</div>'
        + '</div>';
      });
      $(".CamposPruebaQuery").html(Html);
      $("#m-probar-query").modal("show");
    }
    else if($("#IdBaseDatos"+IdTabla).val() == ""){
      $("#MensajeError").html("Debe seleccionar la base de datos a la que conecta la consulta.");
      $("#m-error").modal("show");
    }
    else if (Sql == "") {
      $("#MensajeError").html("Debe ingresar la consulta a probar.");
      $("#m-error").modal("show");
    }
  });

  $("#frmProbarQuery").on('change', '.CampoPrueba', function(event) {
    event.preventDefault();

    var Sql = $(this).parents(".CamposPruebaQuery").children(".Sql").val();
    $.each($(".CampoPrueba"), function(){
      if($(this).val() != ""){
        Sql = Sql.replaceAll("{"+$(this).attr('name')+"}",$(this).val());
      }
    });
    $("#VistaSql").html(Sql);
  });

  $("#frmProbarQuery").on('submit', function(event) {
    event.preventDefault();

    $.ajax({
      url: '../ajax/probar-consulta-sql.php',
      type: 'POST',
      dataType: 'html',
      data: $("#frmProbarQuery").serializeArray()
    })
    .done(function(rjson) {
      $(".CamposPruebaQuery").append('<pre class="alert alert-info">'
        + '<button type="button" class="close" data-dismiss="alert">×</button>'
        + rjson
      + '</pre>');
    })
    .fail(function(rerror) {
      $(".CamposPruebaQuery").append('<pre class="alert alert-warning">'
        + '<button type="button" class="close" data-dismiss="alert">×</button>'
        + rerror
      + '</pre>');
    });

  });
});

function validarPermitidas(Sql){

  var NoPermitidas = "ALTER|GRANT|CREATE|DELETE|DROP|EXEC(UTE)|INSERT( +INTO)|UPDATE|DELETE";
  var ExpresionRegular = new RegExp(NoPermitidas);

  if(ExpresionRegular.test(Sql.toUpperCase())){
    $("#MensajeError").html("<p>No se permite el uso de los siguientes comandos: <br />"
      +NoPermitidas.split("|").join("<br />")+"</p>");
    $("#m-error").modal("show");
    return false;
  }
  else{
    return true;
  }
}

function obtenerVariablesQuery(Sql){

  var TxtVar = "";
  var Tomar = false;
  var Variables = new Array();
  var v = 0;

  for(i=0; i<Sql.length; i++){
    if (Sql.charAt(i) == '{'){
      Tomar = true;
    }
    else if (Sql.charAt(i) == '}') {
      Tomar = false;
      Variables[v] = TxtVar;
      v++;
      TxtVar = "";
    }
    if(Tomar == true && Sql.charAt(i) != '{'){
      TxtVar += Sql.charAt(i);
    }
  }
  return Variables.filter(function (e, i, arr) {
    return Variables.lastIndexOf(e) === i;
  });
}
