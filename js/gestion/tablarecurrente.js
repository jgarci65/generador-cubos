$(function(){

  $("#btnConfigurarInforme").on('click', function(event) {
    event.preventDefault();

    if ($("#Cliente").val() != '' && $("#Informe").val() != '') {

      $(".EspacioInforme").html("");
      consultaTabla(".EspacioInforme", $("#Cliente").val(), $("#Informe").val(), 1);
      $("#btnAgregarTabla").css('display', 'inline');
    }
  });

  $(".EspacioInforme").on('click', '.btnAgregaFilaConfigColumnas', function(event) {
    event.preventDefault();

    $(this).parents("div[id^=ConfigColumnas]").children(".panel-body").append(agregarFilaConfColumnas('', ''));
  });

  $(".EspacioInforme").on('click', '.btnEliminaFilaConfigColumnas', function(event) {
    event.preventDefault();

    $(this).parents("div[class=form-group]").remove();
  });

  $(".EspacioInforme").on('click', '.btnSubeFilaConfigColumnas, .btnBajaFilaConfigColumnas', function(event) {
    event.preventDefault();

    var FormGroup = $(this).parents(".form-group:first");
    if($(this).hasClass('btnSubeFilaConfigColumnas') == true){
      FormGroup.insertBefore(FormGroup.prev());
    }
    else if ($(this).hasClass('btnBajaFilaConfigColumnas') == true) {
      FormGroup.insertAfter(FormGroup.next());
    }
  });

  $(".EspacioInforme").on('click', '.btnAgregaFilaConfigEventos', function(event) {
    event.preventDefault();

    $(this).parents("div[id^=ConfigEventos]").children(".panel-body").append(agregarFilaConfEventos('', '', '', '', ''));
  });

  $(".EspacioInforme").on('click', '.btnEliminaFilaConfigColumnas', function(event) {
    event.preventDefault();

    $(this).parents("div[class=form-group]").remove();
  });

  $(".EspacioInforme").on('click', '.btnSubNivelTabla', function(event) {
    event.preventDefault();

    if($(this).children('span').hasClass('glyphicon-chevron-down') == true){
      var IdTabla = $(this).parents("form").children('.IdTabla').val();
      var Nivel = parseInt($(this).parents("form").children('.Nivel').val()) + 1;
      consultaTabla(".SubInforme"+IdTabla, null, null, Nivel, null, IdTabla);
      $(this).children('span').removeClass('glyphicon-chevron-down').addClass('glyphicon-chevron-up');
    }
    else{
      $(this).children('span').removeClass('glyphicon-chevron-up').addClass('glyphicon-chevron-down');
      $(".SubInforme"+$(this).parents("form").children('.IdTabla').val()).html("");
    }
  });

    $(".EspacioInforme").on('submit', 'form[id^=frmTabla]', function(event) {
    event.preventDefault();

    var IdTabla = $(this).children('.IdTabla').val();
    var Sql = $("#Sql"+IdTabla).val();
    if(validarPermitidas(Sql) == true){
      
      $.ajax({
        url: '../ajax/gestion/tablarecurrente.php',
        type: 'POST',
        dataType: 'json',
        data: $(this).serializeArray()
      })
      .done(function(rjson) {
        if (rjson.Cambios == true) {
          alert("Se procesaron los cambios exitosamente.");
        }
        else{
          alert("No se encontraron cambios que procesar.");
        }
      })
      .fail(function(error) {
        $("#MensajeError").html(error.responseText);
        $("#m-error").modal("show");
      });
    }
  });

  $(".EspacioInforme").on('click', '.btnDeshacerCambiosTabla', function(event) {
    event.preventDefault();

    var IdTabla = $(this).parents("form").children('.IdTabla').val();
    var Nivel = $(this).parents("form").children('.Nivel').val();
    var IdPadre = $(this).parents("form").children('.IdPadre').val();

    DivUbica = (Nivel = 1) ? ".EspacioInforme" : ".SubInforme"+IdPadre;
    $("#frmTabla"+IdTabla).remove();
    consultaTabla(DivUbica, null, null, null, IdTabla);
    alert("Se cargo la información almacenada previamente.");
  });

  $(".EspacioInforme").on('click', '.btnDuplicarTabla', function(event) {
    event.preventDefault();

    $("#IdTablaCopiar").val($(this).parents("form").children('.IdTabla').val());
    $("#ClientePegarDT").creaSelect(jsonClientes);
    document.getElementById("frmDuplicarTabla").reset();
    $("#m-duplicar-tabla").modal("show");
  });

  $("#ClientePegarDT").on('change', function(event) {
    event.preventDefault();

    var IdCliente = $(this).val();
    if(IdCliente != ''){
      informesCliente(IdCliente, 'InformePegar');
    }
    else{
      $("#InformePegar").html('');
    }
  });

  $("#frmDuplicarTabla").on('submit', function(event) {
    event.preventDefault();

    if(($("#NivelPegar").val() > 1 && $("#IdTablaPadrePegar").val() != '') || ($("#NivelPegar").val() == 1)){

      $.ajax({
        url: '../ajax/gestion/tablarecurrente.php',
        type: 'POST',
        dataType: 'json',
        data: $("#frmDuplicarTabla").serializeArray()
      })
      .done(function(rjson) {
        if(rjson.Duplicadas > 0){
          alert("Se duplico "+rjson.Duplicadas+" tabla"+((rjson.Duplicadas > 1) ? "s" : "")+" exitosamente.");
          document.getElementById("frmDuplicarTabla").reset();
          $("#m-duplicar-tabla").modal("hide");
        }
        else{
          alert("Algo salio mal, no se ha duplicado ninguna tabla.");
        }
      })
      .fail(function(error) {
        $("#MensajeError").html(error.responseText);
        $("#m-error").modal("show");
      });

    }
    else {
      alert("Debe seleccionar la tabla padre.");
    }
  });

  $(".EspacioInforme").on('click', '.btnEliminarTabla', function(event) {
    event.preventDefault();
    if (window.confirm("Realmente quiere eliminar la consulta?")) { 
        var IdTabla = $(this).parents("form").children('.IdTabla').val();
        $.ajax({
          url: '../ajax/gestion/tablarecurrente.php',
          type: 'POST',
          dataType: 'html',
          data: {
            'IdTabla': IdTabla,
            'Cliente': $("#Cliente").val(),
            'Informe': $("#Informe").val(),
            'Hacer': 'eliminar'
          }
        })
        .done(function(rjson) {
          $("#divTabla"+IdTabla).html("");
          alert("Tabla eliminada exitosamente.");
        })
        .fail(function(error) {
          $("#MensajeError").html(error.responseText);
          $("#m-error").modal("show");
        });
    }
  });

  $(".EspacioInforme").on('click', '.btnAgregarTabla', function(event) {
    event.preventDefault();

    var IdTabla = $(this).parents("form").children('.IdTabla').val();
    var DondeEstoy = this;
    $.ajax({
      url: '../ajax/gestion/tablarecurrente.php',
      type: 'POST',
      dataType: 'json',
      data: {
        'Cliente': $("#Cliente").val(),
        'Informe': $("#Informe").val(),
        'Nivel': (parseInt($(this).parents("form").children('.Nivel').val())+1),
        'IdPadre': IdTabla,
        'Hacer': 'insertar'
      }
    })
    .done(function(rjson) {

      if($(DondeEstoy).parents(".btn-group:first").children('.btnSubNivelTabla').size() == 0){
        $('<button type="button" class="btn btn-default btnSubNivelTabla" title="Consultar subnivel"><span class="glyphicon glyphicon-chevron-up"></span></button>').insertBefore($(DondeEstoy));
      }
      consultaTabla(
        ".SubInforme"+IdTabla,
        null,
        null,
        null,
        rjson.IdTabla,
        null
      );
      alert("Sub nivel agregado exitosamente.");
    })
    .fail(function(error) {
      $("#MensajeError").html(error.responseText);
      $("#m-error").modal("show");
    });

  });
});

function consultaTabla(DivUbica, Cliente = null, Informe = null, Nivel = null, IdTabla = null, IdPadre = null){

  $.ajax({
    url: '../ajax/gestion/tablarecurrente.php',
    type: 'POST',
    dataType: 'json',
    data: {
      'Cliente': Cliente,
      'Informe': Informe,
      'Nivel': Nivel,
      'IdTabla': IdTabla,
      'IdPadre': IdPadre,
      'Hacer': 'consultar'
    }
  })
  .done(function(rjson) {
    if (rjson.Exitoso == true) {
      var CantidadTablas = 0;
      var IdUltimaTabla;
      $.each(rjson.Tablas, function(idx, tbl) {
        $(""+DivUbica).configuradorReporte(tbl);
        CantidadTablas++;
        IdUltimaTabla = tbl.IdTabla;
      });
      if(parseInt(CantidadTablas) == 1){
        irAAncla("Ancla"+IdUltimaTabla);
      }
    }
    else{
      alert(rjson.Mensaje);
    }
  })
  .fail(function(error) {
    $("#MensajeError").html(error.responseText);
    $("#m-error").modal("show");
  });
}

function irAAncla(Ancla){
  $('html, body').stop().animate({
    scrollTop: jQuery('#'+Ancla).offset().top
  }, 1000);
}
