$(function(){

  $("#Informe").on('change', function(event) {
    event.preventDefault();

    $(".EspacioInforme").html("");
    $("#btnAgregarTabla").css('display', 'none');
  });

  $("#btnCreaInforme, #btnEditaInforme, #btnEliminaInforme").on('click', function(event) {
    event.preventDefault();
    document.getElementById("frmGestionInforme").reset();

    if($("#Cliente").val() != ''){
      var Seguir = false;
      if($(this).attr('id') == 'btnCreaInforme'){
        Hacer = 'crear';
        $("#IdInformeGI").val('');
        Seguir = true;
      }
      else if ($(this).attr('id') == 'btnEditaInforme' && $("#Informe").val() != '') {
        Hacer = 'editar';
        $("#NombreInforme").val($("#Informe option:selected").html());
        $("#IdInformeGI").val($("#Informe").val());
        Seguir = true;

      }
      else if ($(this).attr('id') == 'btnEliminaInforme' && $("#Informe").val() != '') {
        Hacer = 'eliminar';
        $("#NombreInforme").val($("#Informe option:selected").html());
        $("#IdInformeGI").val($("#Informe").val());
        Seguir = true;
      }

      if(Seguir == true){
        $("#HacerInforme").val(Hacer);
        $("#btnSubmitInforme").html(Hacer.charAt(0).toUpperCase() + Hacer.slice(1));
        $("#DivNombreClienteEnCR").html($("#Cliente option:selected").html());
        $("#ClienteEnCR").val($("#Cliente").val());
        $("#m-gestion-informe").modal("show");
      }
      else{
        alert("Debe seleccionar el informe a "+Hacer);
      }
    }
    else{
      alert("Debe seleccionar un cliente.");
    }
  });

  $("#frmGestionInforme").on('submit', function(event) {
    event.preventDefault();

    $.ajax({
      url: '../ajax/gestion/informe.php',
      type: 'POST',
      dataType: 'json',
      data: $("#frmGestionInforme").serialize()
    })
    .done(function(rjson) {
      if(rjson.Exitoso == true){
        $("#Informe").creaSelect(rjson.JsonInformes);
        if(rjson.Hacer != 'eliminar'){
          $("#Informe").val(rjson.IdInforme);
        }
        $(".EspacioInforme").html("");
        alert(rjson.Mensaje);
        $("#m-gestion-informe").modal("hide");
        $("#btnAgregarTabla").css('display', 'none');
      }
      else{
        alert("No se pudo realizar el proceso: "+rjson.Mensaje);
      }
    })
    .fail(function(error) {
      $("#MensajeError").html(error.responseText);
      $("#m-error").modal("show");
    });
  });

  $("#btnDuplicaInforme").on('click', function(event) {
    event.preventDefault();

    if($("#Cliente").val() != '' && $("#Informe").val() != ''){

      $("#IdInformeCopiar").val($("#Informe").val());
      $("#NombreInformeCopiar").html($("#Informe option:selected").html());
      $("#ClientePegar").creaSelect(jsonClientes);
      $("#m-duplicar-informe").modal("show");
    }
    else{
      alert("Debe seleccionar el informe a duplicar.");
    }
  });

  $("#InformePegar").on('change', function(event) {
    event.preventDefault();

    if($("#InformePegar").val() != '' && $("#ClientePegarDT").val() != ''){

      $.ajax({
        url: '../ajax/consulta-nivelesytablas-informe.php',
        type: 'GET',
        dataType: 'json',
        data: {
          'Informe': $("#InformePegar").val(),
          'Cliente': $("#ClientePegarDT").val()
        }
      })
      .done(function(rjson) {

        jsonNivelesYTablas = rjson;

        var HtmlNiveles = "<option></option>"
        + '<option value="1">1</option>';
        if($(rjson).size() > 0){
          $.each(rjson, function(Nivel, Tablas) {

            SiguienteNivel = parseInt(Nivel)+1;
            HtmlNiveles += '<option value="'+SiguienteNivel+'">'+SiguienteNivel+'</option>';
          });
        }
        $("#NivelPegar").html(HtmlNiveles);
      })
      .fail(function(error) {
        alert("Ocurrio un error, actualice la página y vuelva a intentarlo, en caso de persistir el problema contacte al administrador del sistema.");
      });
    }
  });

  $("#NivelPegar").on('change', function(event) {
    event.preventDefault();

    var HtmlTablas = "";
    var Nivel = parseInt($("#NivelPegar").val()) - 1;
    if(Nivel > 0 && $(jsonNivelesYTablas[Nivel]).size() > 0){
      $.each(jsonNivelesYTablas[Nivel], function(i, Tabla) {
        HtmlTablas += '<option value="'+Tabla.id_tablarecurrente+'">'+Tabla.descripcion+'</option>';
      });
      $("#IdTablaPadrePegar").attr('required', 'required');
    }
    else{
      $("#IdTablaPadrePegar").removeAttr('required');
    }
    $("#IdTablaPadrePegar").html(HtmlTablas);
  });

  $("#btnAgregarTabla").on('click', function(event) {
    event.preventDefault();

    $.ajax({
      url: '../ajax/gestion/tablarecurrente.php',
      type: 'POST',
      dataType: 'json',
      data: {
        'Cliente': $("#Cliente").val(),
        'Informe': $("#Informe").val(),
        'Nivel': 1,
        'IdPadre': null,
        'Hacer': 'insertar'
      }
    })
    .done(function(rjson) {

      consultaTabla(
        ".EspacioInforme",
        null,
        null,
        null,
        rjson.IdTabla,
        null
      );
      alert("Nivel agregado exitosamente.");

    })
    .fail(function(error) {
      $("#MensajeError").html(error.responseText);
      $("#m-error").modal("show");
    });
  });

  $("#frmDuplicarInforme").on('submit', function(event) {
    event.preventDefault();

    $.ajax({
      url: '../ajax/gestion/tablarecurrente.php',
      type: 'POST',
      dataType: 'json',
      data: $("#frmDuplicarInforme").serializeArray()
    })
    .done(function(rjson) {
      if(rjson.Exitoso == true){
        $("#m-duplicar-informe").modal("hide");
        document.getElementById("frmDuplicarInforme").reset();
        $("#Cliente").change();
      }
      alert(rjson.Mensaje);
    })
    .fail(function(error) {
      $("#MensajeError").html(error.responseText);
      $("#m-error").modal("show");
    });
  });

  $("#btnProbarInforme").on('click', function(event) {
    event.preventDefault();

    var IdInforme = $("#Informe").val();
    if(IdInforme != null && IdInforme != ''){
      $.ajax({
        url: '../ajax/consulta-estado-general-informe.php',
        type: 'GET',
        dataType: 'json',
        data: {
          'Informe': IdInforme,
          'Cliente': $("#Cliente").val()
        }
      })
      .done(function(rjson) {
        if($(rjson.Errores).size() > 0){
          $("#ErroresInforme").html(rjson.Errores.join("<br />")+ "<br /><strong>Solucione los problemas y vuelva a intentarlo</strong>");
          $("#m-errores-informe").modal("show");
        }
        else{
          location.href="probar-reporte.php?IdInforme="+IdInforme;
        }
      })
      .fail(function(error) {
        $("#MensajeError").html(error.responseText);
        $("#m-error").modal("show");
      });
    }
    else{
      alert("Seleccione el informe a probar.");
    }
  });
});

var jsonNivelesYTablas;
