$(function(){

  crearFormularioBusqueda();
});

function crearFormularioBusqueda(){

  Html = '<input type="hidden" name="SiguienteNivel" value="1" />'
  + '<input type="hidden" name="Informe" class="Informe" value="'+jsonDatosInforme[0].id_informe+'" />'
  + '<input type="hidden" name="Cliente" class="Cliente" value="'+jsonDatosInforme[0].id_cliente+'" />';

  if($(jsonVariables).size() > 0){

    $.each(jsonVariables, function(idx, Va) {
      Html += '<div class="form-group">'
        + '<label for="'+Va+'" class="col-md-3">'+Va+'</label>'
        + '<div class="col-md-9">'
          + '<input type="text" class="form-control CampoPrueba" name="'+Va+'" placeholder="">'
        + '</div>'
      + '</div>';
    });
  }

  Html += '<button type="submit" class="btn btn-default" id="btnNivel0">'
    + '<span class="glyphicon glyphicon-search"> </span> Buscar'
  + '</button>';

  $("#frmNivel0").html(Html);
}
