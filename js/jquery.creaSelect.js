$(function(){

  $.fn.creaSelect = function(JsonSelect){

    var HtmlSelect = "<option></option>";
    $.each(JsonSelect, function(idx, cont) {
      HtmlSelect += "<option value='"+cont['Llave']+"'>"+cont['Contenido']+"</option>";
    });
    $(this).html(HtmlSelect);
  };

  $.creaSelect = function(JsonSelect, LlaveSelected){

    var HtmlSelect = "<option></option>";
    $.each(JsonSelect, function(idx, cont) {
      HtmlSelect += "<option value='"+cont['Llave']+"' "+((LlaveSelected == cont['Llave']) ? "selected='selected'" : "")+">"+cont['Contenido']+"</option>";
    });
    return HtmlSelect;
  }
});
