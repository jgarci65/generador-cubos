(function($){

  $.fn.configuradorReporte = function(Opciones){

    var Configuracion = {
      Tabla: new Array(),
      Campos: new Array(),
      IdTabla: null,
      IdTablaPadre: null,
      TablaHija: 0,
      Nivel: null,
    };
    $.extend(Configuracion, Opciones);

    function _fnCrearInformacionBasica(){

      var Html = ''
      + '<input type="hidden" class="IdTabla" name="IdTabla" value="'+Configuracion.IdTabla+'">'
      + '<input type="hidden" class="IdPadre" name="IdPadre" value="'+Configuracion.IdPadre+'">'
      + '<input type="hidden" name="TablaHija" value="'+Configuracion.TablaHija+'">'
      + '<input type="hidden" class="Nivel" name="Nivel" value="'+Configuracion.Nivel+'">'
      + '<input type="hidden" name="Hacer" class="HacerTabla" value="guardar">'
      + '<div class="panel panel-default">'
        + '<div class="panel-heading">'
          + '<h3 class="panel-title"><strong>INFORMACIÓN BASICA</strong></h3>'
        + '</div>'
        + '<div class="panel-body">'
          + '<div class="form-group">'
            + '<label for="IdBaseDatos" class="col-md-2">Base de datos</label>'
            + '<div class="col-md-8">'
              + '<select class="form-control BaseDatos" name="IdBaseDatos" id="IdBaseDatos'+Configuracion.IdTabla+'" required="required">'
               + $.creaSelect(jsonBaseDatos, Configuracion.IdBaseDatos)
              + '</select>'
            + '</div>'
            + '<div class="col-md-2">'
              + '<div class="btn-group btn-group-sm">'
                + '<button type="button" class="btn btn-default btnCreaBaseDatos" title="Crear">'
                  + '<span class="glyphicon glyphicon-plus"></span>'
                + '</button>'
                + '<button type="button" class="btn btn-default btnEditaBaseDatos" title="Editar">'
                  + '<span class="glyphicon glyphicon-edit"></span>'
                + '</button>'
                + '<button type="button" class="btn btn-default btnEliminaBaseDatos" title="Eliminar">'
                  + '<span class="glyphicon glyphicon-minus"></span>'
                + '</button>'
              + '</div>'
            + '</div>'
          + '</div>'
          + '<div class="form-group">'
            + '<label for="Descripcion" class="col-md-2">Descripción</label>'
            + '<div class="col-md-10">'
              + '<input type="text" class="form-control" name="Descripcion" placeholder="" value="'+Configuracion.Descripcion+'" required="required">'
            + '</div>'
          + '</div>'
          + '<div class="form-group">'
            + '<label for="Sql" class="col-md-2">Sql</label>'
            + '<div class="col-md-9">'
              + '<textarea style="width:100%; height:100%;" name="Sql" id="Sql'+Configuracion.IdTabla+'" rows="4" class="form-control Sql" required="required">'+Configuracion.Sql+'</textarea>'
            + '</div>'
            + '<div class="col-md-1">'
              + '<div class="btn-group">'
                + '<button type="button" class="btn btn-default btnProbarQuery" title="Probar consulta">'
                  + '<span class="glyphicon glyphicon-play"></span>'
                + '</button>'
              + '</div>'
            + '</div>'
          + '</div>'
        + '</div>'
      + '</div>';
      return Html;
    }

    function _fnCrearConfiguracionColumnas(){

      var Html = ''
        + '<div class="panel panel-default" id="ConfigColumnas'+Configuracion.IdTabla+'">'
          + '<div class="panel-heading">'
            + '<h3 class="panel-title"><strong>CONFIGURACIÓN DE LAS COLUMNAS</strong></h3>'
          + '</div>'
          + '<div class="panel-body">';

      if($(Configuracion.Campos).size() > 0){

        $.each(Configuracion.Campos, function(idx, ele) {
          Html += agregarFilaConfColumnas(idx, ele);
        });
      }
      Html += ''
          + '</div>'
          + '<div class="panel-footer">'
            + '<button type="button" class="btn btn-default btnAgregaFilaConfigColumnas" title="Agregar">'
              + '<span class="glyphicon glyphicon-plus"></span>'
            + '</button>'
          + '</div>'
        + '</div>';
      return Html;
    }

    function _fnCrearConfiguracionEventos(){

      var Html = '<div class="panel panel-default" id="ConfigEventos'+Configuracion.IdTabla+'">'
        + '<div class="panel-heading">'
          + '<h3 class="panel-title"><strong>CONFIGURACIÓN DE LOS EVENTOS</strong></h3>'
        + '</div>'
        + '<div class="panel-body">';

      if($(Configuracion.Eventos).size() > 0){
        $.each(Configuracion.Eventos, function(idx, ele) {
          Html += agregarFilaConfEventos(ele.campo, ele.evento, ele.descripcion, ele.id_tipo, ele.icono);
        });
      }

      Html += ''
        + '</div>'
        + '<div class="panel-footer">'
          + '<button type="button" class="btn btn-default btnAgregaFilaConfigEventos" title="Agregar">'
            + '<span class="glyphicon glyphicon-plus"></span>'
          + '</button>'
        + '</div>'
      + '</div>';
      return Html;
    }

    function _fnCrearBotonera(){

      var Html = '<div class="col-md-12 btn-group">'
        + '<button type="submit" class="btn btn-default btnGuardarCambiosTabla" title="Guardar cambios">'
          + '<span class="glyphicon glyphicon-floppy-disk"></span>'
        + '</button>'
        + '<button type="button" class="btn btn-default btnDeshacerCambiosTabla" title="Deshacer cambios">'
          + '<span class="glyphicon glyphicon-repeat"></span>'
        + '</button>'
        + '<button type="button" class="btn btn-default btnDuplicarTabla" title="Duplicar tabla">'
          + '<span class="glyphicon glyphicon-duplicate"></span>'
        + '</button>'
        + '<button type="button" class="btn btn-default btnEliminarTabla" title="Eliminar tabla">'
          + '<span class="glyphicon glyphicon-trash"></span>'
        + '</button>'
        + ((Configuracion.TablaHija > 0) ? '<button type="button" class="btn btn-default btnSubNivelTabla" title="Consultar subnivel"><span class="glyphicon glyphicon-chevron-down"></span></button>' : '')
        + '<button type="button" class="btn btn-default btnAgregarTabla" title="Agregar subnivel">'
          + '<span class="glyphicon glyphicon-plus"></span>'
        + '</button>'
      + '</div>';
      return Html;
    }

    this.each(function() {

      var Html = _fnCrearInformacionBasica();
      Html += _fnCrearConfiguracionColumnas();
      Html += _fnCrearConfiguracionEventos();
      Html += _fnCrearBotonera();

      if ( !$("#divTabla"+Configuracion.IdTabla).length ) {
        $(this).append('<div class="panel-group" id="divTabla'+Configuracion.IdTabla+'">'
          + '<div class="panel panel-primary">'
            + '<div class="panel-heading">'
              + '<h4 class="panel-title">'
                + '<a data-toggle="collapse" data-parent="#divTabla'+Configuracion.IdTabla+'" id="Ancla'+Configuracion.IdTabla+'" href="#accordion'+Configuracion.IdTabla+'">NIVEL '+Configuracion.Nivel+'</a>'
              + '</h4>'
            + '</div>'
            + '<div id="accordion'+Configuracion.IdTabla+'" class="panel-collapse collapse in">'
              + '<div class="panel-body"></div>'
            + '</div>'
         + '</div>'
       + '</div>');
      }
      $("#accordion"+Configuracion.IdTabla+" > .panel-body").html(
        '<form class="form-horizontal" method="post" id="frmTabla'+Configuracion.IdTabla+'">'
          + Html
        + "</form>"
        + '<div class="col-md-12 SubInforme'+Configuracion.IdTabla+'"></div>'
      );
      autosize($('textarea'));
    });
  };
})(jQuery);

function agregarFilaConfColumnas(NombreSql, NombreColumna){

  var Html = ''
  + '<div class="form-group">'
    + '<div class="col-md-5 ColumnasSql">'
      + '<label for="NombreSqlaVista">Columna en el sql</label>'
      + '<div class="">'
        + '<input type="text" class="form-control" name="NombreSqlaVista[]" value="'+NombreSql+'" required="required">'
      + '</div>'
    + '</div>'
    + '<div class="col-md-5" id="CampposVista">'
      + '<label for="NombreEnVista">Columna en la vista</label>'
      + '<div class="">'
        + '<input type="text" class="form-control" name="NombreEnVista[]" value="'+NombreColumna+'" required="required">'
      + '</div>'
    + '</div>'
    + '<div class="col-md-2">'
      + '<label for=""></label>'
      + '<div class="">'
        + '<div class="btn-group">'
          + '<button type="button" class="btn btn-default btnEliminaFilaConfigColumnas" title="Eliminar">'
            + '<span class="glyphicon glyphicon-minus"></span>'
          + '</button>'
          + '<button type="button" class="btn btn-default btnSubeFilaConfigColumnas" title="Subir">'
            + '<span class="glyphicon glyphicon-arrow-up"></span>'
          + '</button>'
          + '<button type="button" class="btn btn-default btnBajaFilaConfigColumnas" title="Bajar">'
            + '<span class="glyphicon glyphicon-arrow-down"></span>'
          + '</button>'
        + '</div>'
      + '</div>'
    + '</div>'
  + '</div>';
  return Html;
}

function agregarFilaConfEventos(NombreSql, NombreEvento, Descripcion, TipoEvento, Icono){

  var Html = ''
  + '<div class="form-group">'
    + '<div class="col-md-2">'
      + '<label for="NombreSqlaEvento">Campo en el sql</label>'
      + '<input type="text" class="form-control" name="NombreSqlaEvento[]" value="'+NombreSql+'" required="required">'
    + '</div>'
    + '<div class="col-md-2">'
      + '<label for="NombreEvento">Nombre función</label>'
      + '<input type="text" class="form-control" name="NombreEvento[]" value="'+NombreEvento+'" required="required">'
    + '</div>'
    + '<div class="col-md-2">'
      + '<label for="DescripcionEvento">Descripción</label>'
      + '<input type="text" class="form-control" name="DescripcionEvento[]" value="'+Descripcion+'">'
    + '</div>'
    + '<div class="col-md-2">'
      + '<label for="TipoEvento">Tipo evento</label>'
      + '<select class="form-control" name="TipoEvento[]" required="required">'
      + $.creaSelect(jsonEventos, TipoEvento)
      + '</select>'
    + '</div>'
    + '<div class="col-md-3">'
      + '<label for="IconoEvento">Icono</label>'
      + '<div class="input-group">'
        + '<input type="text" class="form-control" name="IconoEvento[]" value="'+Icono+'" required="required">'
        + '<a href="http://www.w3schools.com/bootstrap/bootstrap_ref_comp_glyphs.asp" target="_blank" class="input-group-addon glyphicon glyphicon-info-sign"></a>'
      + '</div>'
    + '</div>'
    + '<div class="col-md-1">'
      + '<label for=""></label>'
      + '<div class="">'
        + '<div class="btn-group">'
          + '<button type="button" class="btn btn-default btnEliminaFilaConfigColumnas" title="Eliminar">'
            + '<span class="glyphicon glyphicon-minus"></span>'
          + '</button>'
        + '</div>'
      + '</div>'
    + '</div>'
  + '</div>';
  return Html;
}
