$(document).ready(function (){
    $("#tabs").tabs();
    $(".mti-tabs").show();
    $("#enviar").click(function (){
        if($("#idxmcred").val().length>0&&$("#idxdoc").val().length>0)
            $("#imagen").submit();
    });
    $("#imagen").submit(function (){
        if($("#archivo").val().length==0)
            return false;
        return true;
    });
    $('#buscadordoc, #buscadorcar').blur(function (){
        $(this).val($(this).val().toUpperCase());
    });
    $("#addDoc").click(function (){
        //todo asignar docuemtno
        });
    //TODO mostrar apenas se confirme el credito
    //$("#formUpload").show();
    /**
     *@description abrir buscador
     */
    $("#openBusca").click(function (){
        $("#dialog-buscador").dialog('open');
    });
    /*
     *@description formulario buscador
     **/
    $("#dialog-buscador").dialog({
        autoOpen: false,
        height: 500,
        width: 550,
        modal: true,
        resizable: true,
        buttons: {
            Buscar: function (){
                $("#solBuscador").hide();
                $("#tipoenvioobserbus, #tipoenviobus").removeClass('ui-state-error');
                $para='';
                $para+='&buscadorid='+$("#buscadorid").val();
                $para+='&buscadornc='+$("#buscadornc").val();
                $.post('../php/upload/buscar.php',$para,function (data){
                    $("#buscadorres").html(data+'<br /><br />');
                    formatTable('#buscadortable');
                    activeRadioTable('#buscadortable');
                });
            },
            Seleccionar: function() {
                $tr=getSelectTable('#buscadortable');
                if($($tr).length>0){
                    $("#listfolderres tr td:eq(0)").html($($tr).children().next().html());
                    $("#listfolderres tr td:eq(1)").html($($tr).children().next().next().html());
                    $("#listfolderres tr td:eq(2)").html($($tr).children().next().next().next().html());
                    $("#idxmcred").val($($tr).attr('id'));
                    $("#contrato").val($($tr).children().next().html());
                    $("#idxdoc").val('');
                    $("#sticker").val('');
                    $("#listfolderres tr td:eq(3)").html('');
                    $("#listfolderres tr td:eq(4)").html('');
                    $(this).dialog("close");
                }
            },
            Cancelar: function() {
                $(this).dialog("close");
            }
        },
        close: function() {
            $("#tipoenvioobserbus, #tipoenviobus, #buscadorid, #buscadornc").val('');
            $("#buscadorres").html('');
            $("#solBuscador").hide();
        }
    });
    /**
     *@description abrir buscador
     */
    $("#addDoc").click(function (){
        if($("#idxmcred").val().length>0)
            $("#dialog-documento").dialog('open');
    });
    /*
     *@description formulario buscador
     **/
    $("#dialog-documento").dialog({
        autoOpen: false,
        height: 500,
        width: 650,
        modal: true,
        resizable: true,
        buttons:{
            Buscar: function (){
                $para='';
                $para+='&buscadordoc='+$('#buscadordoc').val().toUpperCase();
                $para+='&buscadorcar='+$('#buscadorcar').val().toUpperCase();
                $.post('../php/upload/getdoc.php',$para,function (data){
                    $('#buscadorresdoc').html(data);
                    formatTable('#tableDocs');
                    activeRadioTable('#tableDocs');
                });
            },
            Seleccionar: function (){
                $tr=getSelectTable('#tableDocs');
                if($tr.length>0){
                    $("#idxdoc").val($($tr).attr('id'));
                    $("#coddoc").val($($tr).children().next().html());
                    $("#listfolderres tr td:eq(3)").html($($tr).children().next().next().html());
                    $("#listfolderres tr td:eq(4)").html('M000140000'+getCeros($($tr).attr('id'),4)+$("#listfolderres tr td:eq(0)").html());
                    $("#sticker").val('M000140000'+getCeros($($tr).attr('id'),4)+$("#listfolderres tr td:eq(0)").html());
                    $(this).dialog('close');
                }
            },
            Cancelar: function() {
                $(this).dialog('close');
            }
        },
        close: function (){
            $("#buscadorresdoc").html('');
            $("#buscadordoc, #buscadorcar").val('');
        }
    });
});
/**
 *@description funcion para retornar cantidad de ceros
 */
function getCeros($num,$can){
    $cad='';
    $can=$num.toString().split('').length;
    for(k=0;k<$can;k++){
        $cad+='0';
    }
    return $cad+$num;
}
/*
 *@description funcion para obtener los seleccionados de una tabla
 **/
function getSelectTable($table){
    $tr=$($table+" .ui-state-highlight");
    return ($tr);
}
/*
 *@description funcion de formato para tabla
 **/
function formatTable($tabla){
    $($tabla+' tr:even').each(function(){
        $(this).addClass('ui-state-default');
    });
    $($tabla+' tr:eq(0)').removeClass('ui-state-default').addClass('ui-widget-header');
    $($tabla+' tr:gt(0)').hover(function (){
        $(this).addClass('ui-state-hover');
    },function (){
        $(this).removeClass('ui-state-hover');
    });
    $($tabla).addClass('normalTable');
    $($tabla+" button").addClass('ui-button ui-widget ui-state-default ui-corner-all').hover(function (){
        $(this).addClass('ui-state-hover');
    },function (){
        $(this).removeClass('ui-state-hover');
    });
}
/*
 *@description funcion de checks para tabla
 **/
function activeCheckTable($chkall,$chkclass){
    $($chkall).click(function (){
        if($(this).attr("checked")==true){
            $($chkclass).each(function (){
                $(this).attr("checked","checked");
                $(this).parent().parent().addClass("ui-state-highlight");
            });
        }
        else{
            $($chkclass).each(function (){
                $(this).removeAttr("checked");
                $(this).parent().parent().removeClass("ui-state-highlight");
            });
        }
    });
    $($chkclass).click(function (){
        $t=$($chkclass).length;
        $c=$($chkclass+":checked").length;
        if($t==$c)
            $($chkall).attr("checked","checked");
        else
            $($chkall).removeAttr("checked");
        if($(this).attr("checked")==true){
            $(this).parent().parent().addClass("ui-state-highlight");
        }
        else{
            $(this).parent().parent().removeClass("ui-state-highlight");
        }
    });
}
/*
 *@description funcion de radios para tabla
 **/
function activeRadioTable($table){
    $($table+" :radio").click(function (){
        $.each($(".ui-state-highlight"),function (){
            $(this).removeClass('ui-state-highlight');
        });
        $(this).parent().parent().addClass('ui-state-highlight');
    });
}

/**
 *@description funcion de buscador para tabla
 **/
function activeBuscadortable($tabla,$input,$clean){
    $($clean).click(function (){
        $($input).val('');
        $($input).keyup();
    });
    $($input).keyup(function (){
        $.uiTableFilter($($tabla),this.value);
    })
    $($clean).click();
}