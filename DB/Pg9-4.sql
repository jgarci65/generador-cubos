--
-- PostgreSQL database dump
--

-- Dumped from database version 9.4.20
-- Dumped by pg_dump version 10.5

-- Started on 2019-05-07 11:12:47 -05

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

DROP DATABASE generador;
--
-- TOC entry 2335 (class 1262 OID 19799)
-- Name: generador; Type: DATABASE; Schema: -; Owner: postgres
--

CREATE DATABASE generador WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'es_ES.UTF-8' LC_CTYPE = 'es_ES.UTF-8';


ALTER DATABASE generador OWNER TO postgres;

\connect generador

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 8 (class 2615 OID 19803)
-- Name: genrep; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA genrep;


ALTER SCHEMA genrep OWNER TO postgres;

--
-- TOC entry 1 (class 3079 OID 12123)
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner:
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- TOC entry 2339 (class 0 OID 0)
-- Dependencies: 1
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner:
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


--
-- TOC entry 198 (class 1255 OID 19938)
-- Name: fn_bi_tablarecurrente_padre(); Type: FUNCTION; Schema: genrep; Owner: postgres
--

CREATE FUNCTION genrep.fn_bi_tablarecurrente_padre() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
  if ((new.id_tablarecurrente_padre is null) or (new.id_tablarecurrente_padre = -1)) then
    select new.id_tablarecurrente into new.id_tablarecurrente_padre;
  end if;
  RETURN NEW;
END;
$$;


ALTER FUNCTION genrep.fn_bi_tablarecurrente_padre() OWNER TO postgres;

--
-- TOC entry 199 (class 1255 OID 19939)
-- Name: fn_reordenar_trm(integer); Type: FUNCTION; Schema: genrep; Owner: postgres
--

CREATE FUNCTION genrep.fn_reordenar_trm(integer) RETURNS text
    LANGUAGE plpgsql
    AS $_$
DECLARE
	vi_id_tabla ALIAS FOR $1;

	r genrep.tablarecurrente_muestra%rowtype;
	i smallint;
BEGIN
	i := 1;
	FOR r IN SELECT * FROM genrep.tablarecurrente_muestra
	WHERE id_tablarecurrente = vi_id_tabla
	ORDER BY orden ASC
	LOOP
		UPDATE genrep.tablarecurrente_muestra
		SET orden = i
		WHERE id_tablarecurrente = r.id_tablarecurrente
			AND campo_query = r.campo_query;
		i := i+1;
	END LOOP;
	RETURN 'Re ordenado';
END;
$_$;


ALTER FUNCTION genrep.fn_reordenar_trm(integer) OWNER TO postgres;

--
-- TOC entry 174 (class 1259 OID 19871)
-- Name: sq_bdd_id_basededatos; Type: SEQUENCE; Schema: genrep; Owner: postgres
--

CREATE SEQUENCE genrep.sq_bdd_id_basededatos
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE genrep.sq_bdd_id_basededatos OWNER TO postgres;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 175 (class 1259 OID 19873)
-- Name: basededatos; Type: TABLE; Schema: genrep; Owner: postgres
--

CREATE TABLE genrep.basededatos (
    id_basededatos smallint DEFAULT nextval('genrep.sq_bdd_id_basededatos'::regclass) NOT NULL,
    nombre character varying(250),
    usuario character varying(250),
    clave character varying(250),
    base_de_datos character varying(250),
    esquema character varying(250),
    host character varying(50),
    driver character varying(10),
    puerto smallint
);


ALTER TABLE genrep.basededatos OWNER TO postgres;

--
-- TOC entry 176 (class 1259 OID 19877)
-- Name: sq_c_id_cliente; Type: SEQUENCE; Schema: genrep; Owner: postgres
--

CREATE SEQUENCE genrep.sq_c_id_cliente
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE genrep.sq_c_id_cliente OWNER TO postgres;

--
-- TOC entry 177 (class 1259 OID 19879)
-- Name: cliente; Type: TABLE; Schema: genrep; Owner: postgres
--

CREATE TABLE genrep.cliente (
    id_cliente integer DEFAULT nextval('genrep.sq_c_id_cliente'::regclass) NOT NULL,
    nombre character varying(100),
    id_cliente_atlas smallint
);


ALTER TABLE genrep.cliente OWNER TO postgres;

--
-- TOC entry 178 (class 1259 OID 19883)
-- Name: sq_ej_id_evento; Type: SEQUENCE; Schema: genrep; Owner: postgres
--

CREATE SEQUENCE genrep.sq_ej_id_evento
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE genrep.sq_ej_id_evento OWNER TO postgres;

--
-- TOC entry 179 (class 1259 OID 19885)
-- Name: evento_javascript; Type: TABLE; Schema: genrep; Owner: postgres
--

CREATE TABLE genrep.evento_javascript (
    id_evento_javascript smallint DEFAULT nextval('genrep.sq_ej_id_evento'::regclass) NOT NULL,
    nombre character varying(20)
);


ALTER TABLE genrep.evento_javascript OWNER TO postgres;

--
-- TOC entry 180 (class 1259 OID 19889)
-- Name: sq_ic_id_informe; Type: SEQUENCE; Schema: genrep; Owner: postgres
--

CREATE SEQUENCE genrep.sq_ic_id_informe
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE genrep.sq_ic_id_informe OWNER TO postgres;

--
-- TOC entry 181 (class 1259 OID 19891)
-- Name: informe_cliente; Type: TABLE; Schema: genrep; Owner: postgres
--

CREATE TABLE genrep.informe_cliente (
    id_informe integer DEFAULT nextval('genrep.sq_ic_id_informe'::regclass) NOT NULL,
    nombre_informe character varying(100),
    id_cliente integer NOT NULL
);


ALTER TABLE genrep.informe_cliente OWNER TO postgres;

--
-- TOC entry 182 (class 1259 OID 19895)
-- Name: sq_tr_id_tablarecurrente; Type: SEQUENCE; Schema: genrep; Owner: postgres
--

CREATE SEQUENCE genrep.sq_tr_id_tablarecurrente
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE genrep.sq_tr_id_tablarecurrente OWNER TO postgres;

--
-- TOC entry 183 (class 1259 OID 19897)
-- Name: tablarecurrente; Type: TABLE; Schema: genrep; Owner: postgres
--

CREATE TABLE genrep.tablarecurrente (
    id_tablarecurrente integer DEFAULT nextval('genrep.sq_tr_id_tablarecurrente'::regclass) NOT NULL,
    descripcion character varying(200),
    sql character varying(9000),
    nivel smallint DEFAULT 1,
    id_tablarecurrente_padre integer,
    id_informe integer NOT NULL,
    id_basededatos smallint
);


ALTER TABLE genrep.tablarecurrente OWNER TO postgres;

--
-- TOC entry 184 (class 1259 OID 19905)
-- Name: tablarecurrente_evento; Type: TABLE; Schema: genrep; Owner: postgres
--

CREATE TABLE genrep.tablarecurrente_evento (
    id_tablarecurrente integer NOT NULL,
    descripcion_usuario character varying(100),
    campo_query character varying(100) NOT NULL,
    evento character varying(100),
    id_evento_javascript smallint NOT NULL,
    icono character varying(30) DEFAULT 'glyphicon-search'::character varying
);


ALTER TABLE genrep.tablarecurrente_evento OWNER TO postgres;

--
-- TOC entry 2352 (class 0 OID 0)
-- Dependencies: 184
-- Name: COLUMN tablarecurrente_evento.icono; Type: COMMENT; Schema: genrep; Owner: postgres
--

COMMENT ON COLUMN genrep.tablarecurrente_evento.icono IS 'Icono se glyphicon puede consultarlos en el siguiente enlace: http://www.w3schools.com/bootstrap/bootstrap_ref_comp_glyphs.asp';


--
-- TOC entry 185 (class 1259 OID 19909)
-- Name: tablarecurrente_muestra; Type: TABLE; Schema: genrep; Owner: postgres
--

CREATE TABLE genrep.tablarecurrente_muestra (
    id_tablarecurrente integer NOT NULL,
    campo_query character varying(100) NOT NULL,
    campo_vista character varying(100),
    orden smallint DEFAULT 1 NOT NULL
);


ALTER TABLE genrep.tablarecurrente_muestra OWNER TO postgres;

--
-- TOC entry 2323 (class 0 OID 19885)
-- Dependencies: 179
-- Data for Name: evento_javascript; Type: TABLE DATA; Schema: genrep; Owner: postgres
--

INSERT INTO genrep.evento_javascript VALUES (1, 'onclick');
INSERT INTO genrep.evento_javascript VALUES (2, 'onchange');

--
-- TOC entry 2355 (class 0 OID 0)
-- Dependencies: 174
-- Name: sq_bdd_id_basededatos; Type: SEQUENCE SET; Schema: genrep; Owner: postgres
--

SELECT pg_catalog.setval('genrep.sq_bdd_id_basededatos', 1, true);


--
-- TOC entry 2356 (class 0 OID 0)
-- Dependencies: 176
-- Name: sq_c_id_cliente; Type: SEQUENCE SET; Schema: genrep; Owner: postgres
--

SELECT pg_catalog.setval('genrep.sq_c_id_cliente', 1, true);


--
-- TOC entry 2357 (class 0 OID 0)
-- Dependencies: 178
-- Name: sq_ej_id_evento; Type: SEQUENCE SET; Schema: genrep; Owner: postgres
--

SELECT pg_catalog.setval('genrep.sq_ej_id_evento', 2, true);


--
-- TOC entry 2358 (class 0 OID 0)
-- Dependencies: 180
-- Name: sq_ic_id_informe; Type: SEQUENCE SET; Schema: genrep; Owner: postgres
--

SELECT pg_catalog.setval('genrep.sq_ic_id_informe', 1, true);


--
-- TOC entry 2359 (class 0 OID 0)
-- Dependencies: 182
-- Name: sq_tr_id_tablarecurrente; Type: SEQUENCE SET; Schema: genrep; Owner: postgres
--

SELECT pg_catalog.setval('genrep.sq_tr_id_tablarecurrente', 1, true);


--
-- TOC entry 2193 (class 2606 OID 19914)
-- Name: basededatos pk_basededatos; Type: CONSTRAINT; Schema: genrep; Owner: postgres
--

ALTER TABLE ONLY genrep.basededatos
    ADD CONSTRAINT pk_basededatos PRIMARY KEY (id_basededatos);


--
-- TOC entry 2195 (class 2606 OID 19916)
-- Name: cliente pk_cliente; Type: CONSTRAINT; Schema: genrep; Owner: postgres
--

ALTER TABLE ONLY genrep.cliente
    ADD CONSTRAINT pk_cliente PRIMARY KEY (id_cliente);


--
-- TOC entry 2197 (class 2606 OID 19918)
-- Name: evento_javascript pk_evento_javascript; Type: CONSTRAINT; Schema: genrep; Owner: postgres
--

ALTER TABLE ONLY genrep.evento_javascript
    ADD CONSTRAINT pk_evento_javascript PRIMARY KEY (id_evento_javascript);


--
-- TOC entry 2200 (class 2606 OID 19920)
-- Name: informe_cliente pk_informe_cliente; Type: CONSTRAINT; Schema: genrep; Owner: postgres
--

ALTER TABLE ONLY genrep.informe_cliente
    ADD CONSTRAINT pk_informe_cliente PRIMARY KEY (id_informe);


--
-- TOC entry 2204 (class 2606 OID 19922)
-- Name: tablarecurrente pk_tablarecurrente; Type: CONSTRAINT; Schema: genrep; Owner: postgres
--

ALTER TABLE ONLY genrep.tablarecurrente
    ADD CONSTRAINT pk_tablarecurrente PRIMARY KEY (id_tablarecurrente);


--
-- TOC entry 2206 (class 2606 OID 19924)
-- Name: tablarecurrente_evento pk_tablarecurrente_evento; Type: CONSTRAINT; Schema: genrep; Owner: postgres
--

ALTER TABLE ONLY genrep.tablarecurrente_evento
    ADD CONSTRAINT pk_tablarecurrente_evento PRIMARY KEY (id_tablarecurrente, campo_query, id_evento_javascript);


--
-- TOC entry 2202 (class 2606 OID 19926)
-- Name: informe_cliente uk_informe_cliente; Type: CONSTRAINT; Schema: genrep; Owner: postgres
--

ALTER TABLE ONLY genrep.informe_cliente
    ADD CONSTRAINT uk_informe_cliente UNIQUE (id_cliente, nombre_informe);


--
-- TOC entry 2198 (class 1259 OID 19927)
-- Name: idx_ic_id_cliente; Type: INDEX; Schema: genrep; Owner: postgres
--

CREATE INDEX idx_ic_id_cliente ON genrep.informe_cliente USING btree (id_cliente);


--
-- TOC entry 2207 (class 2606 OID 19928)
-- Name: tablarecurrente fk_tr_id_informe; Type: FK CONSTRAINT; Schema: genrep; Owner: postgres
--

ALTER TABLE ONLY genrep.tablarecurrente
    ADD CONSTRAINT fk_tr_id_informe FOREIGN KEY (id_informe) REFERENCES genrep.informe_cliente(id_informe) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 2208 (class 2606 OID 19933)
-- Name: tablarecurrente fk_tr_id_tablarecurrente_padre; Type: FK CONSTRAINT; Schema: genrep; Owner: postgres
--

ALTER TABLE ONLY genrep.tablarecurrente
    ADD CONSTRAINT fk_tr_id_tablarecurrente_padre FOREIGN KEY (id_tablarecurrente_padre) REFERENCES genrep.tablarecurrente(id_tablarecurrente) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 2336 (class 0 OID 0)
-- Dependencies: 8
-- Name: SCHEMA genrep; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA genrep FROM PUBLIC;
REVOKE ALL ON SCHEMA genrep FROM postgres;
GRANT ALL ON SCHEMA genrep TO postgres;


--
-- TOC entry 2338 (class 0 OID 0)
-- Dependencies: 6
-- Name: SCHEMA public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- TOC entry 2340 (class 0 OID 0)
-- Dependencies: 198
-- Name: FUNCTION fn_bi_tablarecurrente_padre(); Type: ACL; Schema: genrep; Owner: postgres
--

REVOKE ALL ON FUNCTION genrep.fn_bi_tablarecurrente_padre() FROM PUBLIC;
REVOKE ALL ON FUNCTION genrep.fn_bi_tablarecurrente_padre() FROM postgres;
GRANT ALL ON FUNCTION genrep.fn_bi_tablarecurrente_padre() TO postgres;
GRANT ALL ON FUNCTION genrep.fn_bi_tablarecurrente_padre() TO PUBLIC;


--
-- TOC entry 2341 (class 0 OID 0)
-- Dependencies: 199
-- Name: FUNCTION fn_reordenar_trm(integer); Type: ACL; Schema: genrep; Owner: postgres
--

REVOKE ALL ON FUNCTION genrep.fn_reordenar_trm(integer) FROM PUBLIC;
REVOKE ALL ON FUNCTION genrep.fn_reordenar_trm(integer) FROM postgres;
GRANT ALL ON FUNCTION genrep.fn_reordenar_trm(integer) TO postgres;
GRANT ALL ON FUNCTION genrep.fn_reordenar_trm(integer) TO PUBLIC;


--
-- TOC entry 2342 (class 0 OID 0)
-- Dependencies: 174
-- Name: SEQUENCE sq_bdd_id_basededatos; Type: ACL; Schema: genrep; Owner: postgres
--

REVOKE ALL ON SEQUENCE genrep.sq_bdd_id_basededatos FROM PUBLIC;
REVOKE ALL ON SEQUENCE genrep.sq_bdd_id_basededatos FROM postgres;
GRANT ALL ON SEQUENCE genrep.sq_bdd_id_basededatos TO postgres;


--
-- TOC entry 2343 (class 0 OID 0)
-- Dependencies: 175
-- Name: TABLE basededatos; Type: ACL; Schema: genrep; Owner: postgres
--

REVOKE ALL ON TABLE genrep.basededatos FROM PUBLIC;
REVOKE ALL ON TABLE genrep.basededatos FROM postgres;
GRANT ALL ON TABLE genrep.basededatos TO postgres;


--
-- TOC entry 2344 (class 0 OID 0)
-- Dependencies: 176
-- Name: SEQUENCE sq_c_id_cliente; Type: ACL; Schema: genrep; Owner: postgres
--

REVOKE ALL ON SEQUENCE genrep.sq_c_id_cliente FROM PUBLIC;
REVOKE ALL ON SEQUENCE genrep.sq_c_id_cliente FROM postgres;
GRANT ALL ON SEQUENCE genrep.sq_c_id_cliente TO postgres;


--
-- TOC entry 2345 (class 0 OID 0)
-- Dependencies: 177
-- Name: TABLE cliente; Type: ACL; Schema: genrep; Owner: postgres
--

REVOKE ALL ON TABLE genrep.cliente FROM PUBLIC;
REVOKE ALL ON TABLE genrep.cliente FROM postgres;
GRANT ALL ON TABLE genrep.cliente TO postgres;


--
-- TOC entry 2346 (class 0 OID 0)
-- Dependencies: 178
-- Name: SEQUENCE sq_ej_id_evento; Type: ACL; Schema: genrep; Owner: postgres
--

REVOKE ALL ON SEQUENCE genrep.sq_ej_id_evento FROM PUBLIC;
REVOKE ALL ON SEQUENCE genrep.sq_ej_id_evento FROM postgres;
GRANT ALL ON SEQUENCE genrep.sq_ej_id_evento TO postgres;


--
-- TOC entry 2347 (class 0 OID 0)
-- Dependencies: 179
-- Name: TABLE evento_javascript; Type: ACL; Schema: genrep; Owner: postgres
--

REVOKE ALL ON TABLE genrep.evento_javascript FROM PUBLIC;
REVOKE ALL ON TABLE genrep.evento_javascript FROM postgres;
GRANT ALL ON TABLE genrep.evento_javascript TO postgres;


--
-- TOC entry 2348 (class 0 OID 0)
-- Dependencies: 180
-- Name: SEQUENCE sq_ic_id_informe; Type: ACL; Schema: genrep; Owner: postgres
--

REVOKE ALL ON SEQUENCE genrep.sq_ic_id_informe FROM PUBLIC;
REVOKE ALL ON SEQUENCE genrep.sq_ic_id_informe FROM postgres;
GRANT ALL ON SEQUENCE genrep.sq_ic_id_informe TO postgres;


--
-- TOC entry 2349 (class 0 OID 0)
-- Dependencies: 181
-- Name: TABLE informe_cliente; Type: ACL; Schema: genrep; Owner: postgres
--

REVOKE ALL ON TABLE genrep.informe_cliente FROM PUBLIC;
REVOKE ALL ON TABLE genrep.informe_cliente FROM postgres;
GRANT ALL ON TABLE genrep.informe_cliente TO postgres;


--
-- TOC entry 2350 (class 0 OID 0)
-- Dependencies: 182
-- Name: SEQUENCE sq_tr_id_tablarecurrente; Type: ACL; Schema: genrep; Owner: postgres
--

REVOKE ALL ON SEQUENCE genrep.sq_tr_id_tablarecurrente FROM PUBLIC;
REVOKE ALL ON SEQUENCE genrep.sq_tr_id_tablarecurrente FROM postgres;
GRANT ALL ON SEQUENCE genrep.sq_tr_id_tablarecurrente TO postgres;


--
-- TOC entry 2351 (class 0 OID 0)
-- Dependencies: 183
-- Name: TABLE tablarecurrente; Type: ACL; Schema: genrep; Owner: postgres
--

REVOKE ALL ON TABLE genrep.tablarecurrente FROM PUBLIC;
REVOKE ALL ON TABLE genrep.tablarecurrente FROM postgres;
GRANT ALL ON TABLE genrep.tablarecurrente TO postgres;


--
-- TOC entry 2353 (class 0 OID 0)
-- Dependencies: 184
-- Name: TABLE tablarecurrente_evento; Type: ACL; Schema: genrep; Owner: postgres
--

REVOKE ALL ON TABLE genrep.tablarecurrente_evento FROM PUBLIC;
REVOKE ALL ON TABLE genrep.tablarecurrente_evento FROM postgres;
GRANT ALL ON TABLE genrep.tablarecurrente_evento TO postgres;


--
-- TOC entry 2354 (class 0 OID 0)
-- Dependencies: 185
-- Name: TABLE tablarecurrente_muestra; Type: ACL; Schema: genrep; Owner: postgres
--

REVOKE ALL ON TABLE genrep.tablarecurrente_muestra FROM PUBLIC;
REVOKE ALL ON TABLE genrep.tablarecurrente_muestra FROM postgres;
GRANT ALL ON TABLE genrep.tablarecurrente_muestra TO postgres;


-- Completed on 2019-05-07 11:12:48 -05

--
-- PostgreSQL database dump complete
--
