--
-- PostgreSQL database dump
--

-- Dumped from database version 9.1.23
-- Dumped by pg_dump version 9.5.5

-- Started on 2018-09-27 11:59:30

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 8 (class 2615 OID 31042211)
-- Name: genrep; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA genrep;


ALTER SCHEMA genrep OWNER TO postgres;

SET search_path = genrep, pg_catalog;

--
-- TOC entry 187 (class 1255 OID 31042212)
-- Name: fn_bi_tablarecurrente_padre(); Type: FUNCTION; Schema: genrep; Owner: postgres
--

CREATE FUNCTION fn_bi_tablarecurrente_padre() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
  if ((new.id_tablarecurrente_padre is null) or (new.id_tablarecurrente_padre = -1)) then
    select new.id_tablarecurrente into new.id_tablarecurrente_padre;
  end if;
  RETURN NEW;
END;
$$;


ALTER FUNCTION genrep.fn_bi_tablarecurrente_padre() OWNER TO postgres;

--
-- TOC entry 186 (class 1255 OID 31042219)
-- Name: fn_reordenar_trm(integer); Type: FUNCTION; Schema: genrep; Owner: postgres
--

CREATE FUNCTION fn_reordenar_trm(integer) RETURNS text
    LANGUAGE plpgsql
    AS $_$
DECLARE
	vi_id_tabla ALIAS FOR $1;

	r genrep.tablarecurrente_muestra%rowtype;
	i smallint;
BEGIN
	i := 1;
	FOR r IN SELECT * FROM genrep.tablarecurrente_muestra
	WHERE id_tablarecurrente = vi_id_tabla
	ORDER BY orden ASC
	LOOP
		UPDATE genrep.tablarecurrente_muestra
		SET orden = i
		WHERE id_tablarecurrente = r.id_tablarecurrente
			AND campo_query = r.campo_query;
		i := i+1;
	END LOOP;
	RETURN 'Re ordenado';
END;
$_$;


ALTER FUNCTION genrep.fn_reordenar_trm(integer) OWNER TO postgres;

--
-- TOC entry 164 (class 1259 OID 31042220)
-- Name: sq_bdd_id_basededatos; Type: SEQUENCE; Schema: genrep; Owner: postgres
--

CREATE SEQUENCE sq_bdd_id_basededatos
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sq_bdd_id_basededatos OWNER TO postgres;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 165 (class 1259 OID 31042222)
-- Name: basededatos; Type: TABLE; Schema: genrep; Owner: postgres
--

CREATE TABLE genrep.basededatos (
    id_basededatos smallint DEFAULT nextval('genrep.sq_bdd_id_basededatos'::regclass) NOT NULL,
    nombre character varying(250),
    usuario character varying(250),
    clave character varying(250),
    base_de_datos character varying(250),
    esquema character varying(250),
    host character varying(50),
    driver character varying(10),
    puerto smallint
);


ALTER TABLE basededatos OWNER TO postgres;

--
-- TOC entry 166 (class 1259 OID 31042226)
-- Name: sq_c_id_cliente; Type: SEQUENCE; Schema: genrep; Owner: postgres
--

CREATE SEQUENCE sq_c_id_cliente
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sq_c_id_cliente OWNER TO postgres;

--
-- TOC entry 167 (class 1259 OID 31042228)
-- Name: cliente; Type: TABLE; Schema: genrep; Owner: postgres
--

CREATE TABLE cliente (
    id_cliente integer DEFAULT nextval('sq_c_id_cliente'::regclass) NOT NULL,
    nombre character varying(100),
    id_cliente_atlas smallint
);


ALTER TABLE cliente OWNER TO postgres;

--
-- TOC entry 168 (class 1259 OID 31042232)
-- Name: sq_ej_id_evento; Type: SEQUENCE; Schema: genrep; Owner: postgres
--

CREATE SEQUENCE sq_ej_id_evento
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sq_ej_id_evento OWNER TO postgres;

--
-- TOC entry 169 (class 1259 OID 31042234)
-- Name: evento_javascript; Type: TABLE; Schema: genrep; Owner: postgres
--

CREATE TABLE evento_javascript (
    id_evento_javascript smallint DEFAULT nextval('sq_ej_id_evento'::regclass) NOT NULL,
    nombre character varying(20)
);


ALTER TABLE evento_javascript OWNER TO postgres;

--
-- TOC entry 170 (class 1259 OID 31042238)
-- Name: sq_ic_id_informe; Type: SEQUENCE; Schema: genrep; Owner: postgres
--

CREATE SEQUENCE sq_ic_id_informe
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sq_ic_id_informe OWNER TO postgres;

--
-- TOC entry 171 (class 1259 OID 31042240)
-- Name: informe_cliente; Type: TABLE; Schema: genrep; Owner: postgres
--

CREATE TABLE informe_cliente (
    id_informe integer DEFAULT nextval('sq_ic_id_informe'::regclass) NOT NULL,
    nombre_informe character varying(100),
    id_cliente integer NOT NULL
);


ALTER TABLE informe_cliente OWNER TO postgres;

--
-- TOC entry 172 (class 1259 OID 31042244)
-- Name: sq_tr_id_tablarecurrente; Type: SEQUENCE; Schema: genrep; Owner: postgres
--

CREATE SEQUENCE sq_tr_id_tablarecurrente
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sq_tr_id_tablarecurrente OWNER TO postgres;

--
-- TOC entry 173 (class 1259 OID 31042246)
-- Name: tablarecurrente; Type: TABLE; Schema: genrep; Owner: postgres
--

CREATE TABLE tablarecurrente (
    id_tablarecurrente integer DEFAULT nextval('sq_tr_id_tablarecurrente'::regclass) NOT NULL,
    descripcion character varying(200),
    sql character varying(9000),
    nivel smallint DEFAULT 1,
    id_tablarecurrente_padre integer,
    id_informe integer NOT NULL,
    id_basededatos smallint
);


ALTER TABLE tablarecurrente OWNER TO postgres;

--
-- TOC entry 174 (class 1259 OID 31042254)
-- Name: tablarecurrente_evento; Type: TABLE; Schema: genrep; Owner: postgres
--

CREATE TABLE tablarecurrente_evento (
    id_tablarecurrente integer NOT NULL,
    descripcion_usuario character varying(100),
    campo_query character varying(100) NOT NULL,
    evento character varying(100),
    id_evento_javascript smallint NOT NULL,
    icono character varying(30) DEFAULT 'glyphicon-search'::character varying
);


ALTER TABLE tablarecurrente_evento OWNER TO postgres;

--
-- TOC entry 2792 (class 0 OID 0)
-- Dependencies: 174
-- Name: COLUMN tablarecurrente_evento.icono; Type: COMMENT; Schema: genrep; Owner: postgres
--

COMMENT ON COLUMN tablarecurrente_evento.icono IS 'Icono se glyphicon puede consultarlos en el siguiente enlace: http://www.w3schools.com/bootstrap/bootstrap_ref_comp_glyphs.asp';


--
-- TOC entry 163 (class 1259 OID 31042215)
-- Name: tablarecurrente_muestra; Type: TABLE; Schema: genrep; Owner: postgres
--

CREATE TABLE tablarecurrente_muestra (
    id_tablarecurrente integer NOT NULL,
    campo_query character varying(100) NOT NULL,
    campo_vista character varying(100),
    orden smallint DEFAULT 1 NOT NULL
);


ALTER TABLE tablarecurrente_muestra OWNER TO postgres;

--
-- TOC entry 2323 (class 0 OID 19885)
-- Dependencies: 179
-- Data for Name: evento_javascript; Type: TABLE DATA; Schema: genrep; Owner: postgres
--

INSERT INTO genrep.evento_javascript VALUES (1, 'onclick');
INSERT INTO genrep.evento_javascript VALUES (2, 'onchange');

--
-- TOC entry 2658 (class 2606 OID 31042287)
-- Name: pk_basededatos; Type: CONSTRAINT; Schema: genrep; Owner: postgres
--

ALTER TABLE ONLY basededatos
    ADD CONSTRAINT pk_basededatos PRIMARY KEY (id_basededatos);


--
-- TOC entry 2660 (class 2606 OID 31042289)
-- Name: pk_cliente; Type: CONSTRAINT; Schema: genrep; Owner: postgres
--

ALTER TABLE ONLY cliente
    ADD CONSTRAINT pk_cliente PRIMARY KEY (id_cliente);


--
-- TOC entry 2662 (class 2606 OID 31042292)
-- Name: pk_evento_javascript; Type: CONSTRAINT; Schema: genrep; Owner: postgres
--

ALTER TABLE ONLY evento_javascript
    ADD CONSTRAINT pk_evento_javascript PRIMARY KEY (id_evento_javascript);


--
-- TOC entry 2665 (class 2606 OID 31042294)
-- Name: pk_informe_cliente; Type: CONSTRAINT; Schema: genrep; Owner: postgres
--

ALTER TABLE ONLY informe_cliente
    ADD CONSTRAINT pk_informe_cliente PRIMARY KEY (id_informe);


--
-- TOC entry 2669 (class 2606 OID 31042296)
-- Name: pk_tablarecurrente; Type: CONSTRAINT; Schema: genrep; Owner: postgres
--

ALTER TABLE ONLY tablarecurrente
    ADD CONSTRAINT pk_tablarecurrente PRIMARY KEY (id_tablarecurrente);


--
-- TOC entry 2671 (class 2606 OID 31042298)
-- Name: pk_tablarecurrente_evento; Type: CONSTRAINT; Schema: genrep; Owner: postgres
--

ALTER TABLE ONLY tablarecurrente_evento
    ADD CONSTRAINT pk_tablarecurrente_evento PRIMARY KEY (id_tablarecurrente, campo_query, id_evento_javascript);


--
-- TOC entry 2667 (class 2606 OID 31042316)
-- Name: uk_informe_cliente; Type: CONSTRAINT; Schema: genrep; Owner: postgres
--

ALTER TABLE ONLY informe_cliente
    ADD CONSTRAINT uk_informe_cliente UNIQUE (id_cliente, nombre_informe);


--
-- TOC entry 2663 (class 1259 OID 31042317)
-- Name: idx_ic_id_cliente; Type: INDEX; Schema: genrep; Owner: postgres
--

CREATE INDEX idx_ic_id_cliente ON informe_cliente USING btree (id_cliente);


--
-- TOC entry 2672 (class 2606 OID 31042342)
-- Name: fk_tr_id_informe; Type: FK CONSTRAINT; Schema: genrep; Owner: postgres
--

ALTER TABLE ONLY tablarecurrente
    ADD CONSTRAINT fk_tr_id_informe FOREIGN KEY (id_informe) REFERENCES informe_cliente(id_informe) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 2673 (class 2606 OID 31042347)
-- Name: fk_tr_id_tablarecurrente_padre; Type: FK CONSTRAINT; Schema: genrep; Owner: postgres
--

ALTER TABLE ONLY tablarecurrente
    ADD CONSTRAINT fk_tr_id_tablarecurrente_padre FOREIGN KEY (id_tablarecurrente_padre) REFERENCES tablarecurrente(id_tablarecurrente) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 2779 (class 0 OID 0)
-- Dependencies: 8
-- Name: genrep; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA genrep FROM PUBLIC;
REVOKE ALL ON SCHEMA genrep FROM postgres;
GRANT ALL ON SCHEMA genrep TO postgres;


--
-- TOC entry 2780 (class 0 OID 0)
-- Dependencies: 187
-- Name: fn_bi_tablarecurrente_padre(); Type: ACL; Schema: genrep; Owner: postgres
--

REVOKE ALL ON FUNCTION fn_bi_tablarecurrente_padre() FROM PUBLIC;
REVOKE ALL ON FUNCTION fn_bi_tablarecurrente_padre() FROM postgres;
GRANT ALL ON FUNCTION fn_bi_tablarecurrente_padre() TO postgres;
GRANT ALL ON FUNCTION fn_bi_tablarecurrente_padre() TO PUBLIC;


--
-- TOC entry 2781 (class 0 OID 0)
-- Dependencies: 186
-- Name: fn_reordenar_trm(integer); Type: ACL; Schema: genrep; Owner: postgres
--

REVOKE ALL ON FUNCTION fn_reordenar_trm(integer) FROM PUBLIC;
REVOKE ALL ON FUNCTION fn_reordenar_trm(integer) FROM postgres;
GRANT ALL ON FUNCTION fn_reordenar_trm(integer) TO postgres;
GRANT ALL ON FUNCTION fn_reordenar_trm(integer) TO PUBLIC;


--
-- TOC entry 2782 (class 0 OID 0)
-- Dependencies: 164
-- Name: sq_bdd_id_basededatos; Type: ACL; Schema: genrep; Owner: postgres
--

REVOKE ALL ON SEQUENCE sq_bdd_id_basededatos FROM PUBLIC;
REVOKE ALL ON SEQUENCE sq_bdd_id_basededatos FROM postgres;
GRANT ALL ON SEQUENCE sq_bdd_id_basededatos TO postgres;


--
-- TOC entry 2783 (class 0 OID 0)
-- Dependencies: 165
-- Name: basededatos; Type: ACL; Schema: genrep; Owner: postgres
--

REVOKE ALL ON TABLE basededatos FROM PUBLIC;
REVOKE ALL ON TABLE basededatos FROM postgres;
GRANT ALL ON TABLE basededatos TO postgres;


--
-- TOC entry 2784 (class 0 OID 0)
-- Dependencies: 166
-- Name: sq_c_id_cliente; Type: ACL; Schema: genrep; Owner: postgres
--

REVOKE ALL ON SEQUENCE sq_c_id_cliente FROM PUBLIC;
REVOKE ALL ON SEQUENCE sq_c_id_cliente FROM postgres;
GRANT ALL ON SEQUENCE sq_c_id_cliente TO postgres;


--
-- TOC entry 2785 (class 0 OID 0)
-- Dependencies: 167
-- Name: cliente; Type: ACL; Schema: genrep; Owner: postgres
--

REVOKE ALL ON TABLE cliente FROM PUBLIC;
REVOKE ALL ON TABLE cliente FROM postgres;
GRANT ALL ON TABLE cliente TO postgres;


--
-- TOC entry 2786 (class 0 OID 0)
-- Dependencies: 168
-- Name: sq_ej_id_evento; Type: ACL; Schema: genrep; Owner: postgres
--

REVOKE ALL ON SEQUENCE sq_ej_id_evento FROM PUBLIC;
REVOKE ALL ON SEQUENCE sq_ej_id_evento FROM postgres;
GRANT ALL ON SEQUENCE sq_ej_id_evento TO postgres;


--
-- TOC entry 2787 (class 0 OID 0)
-- Dependencies: 169
-- Name: evento_javascript; Type: ACL; Schema: genrep; Owner: postgres
--

REVOKE ALL ON TABLE evento_javascript FROM PUBLIC;
REVOKE ALL ON TABLE evento_javascript FROM postgres;
GRANT ALL ON TABLE evento_javascript TO postgres;


--
-- TOC entry 2788 (class 0 OID 0)
-- Dependencies: 170
-- Name: sq_ic_id_informe; Type: ACL; Schema: genrep; Owner: postgres
--

REVOKE ALL ON SEQUENCE sq_ic_id_informe FROM PUBLIC;
REVOKE ALL ON SEQUENCE sq_ic_id_informe FROM postgres;
GRANT ALL ON SEQUENCE sq_ic_id_informe TO postgres;


--
-- TOC entry 2789 (class 0 OID 0)
-- Dependencies: 171
-- Name: informe_cliente; Type: ACL; Schema: genrep; Owner: postgres
--

REVOKE ALL ON TABLE informe_cliente FROM PUBLIC;
REVOKE ALL ON TABLE informe_cliente FROM postgres;
GRANT ALL ON TABLE informe_cliente TO postgres;


--
-- TOC entry 2790 (class 0 OID 0)
-- Dependencies: 172
-- Name: sq_tr_id_tablarecurrente; Type: ACL; Schema: genrep; Owner: postgres
--

REVOKE ALL ON SEQUENCE sq_tr_id_tablarecurrente FROM PUBLIC;
REVOKE ALL ON SEQUENCE sq_tr_id_tablarecurrente FROM postgres;
GRANT ALL ON SEQUENCE sq_tr_id_tablarecurrente TO postgres;


--
-- TOC entry 2791 (class 0 OID 0)
-- Dependencies: 173
-- Name: tablarecurrente; Type: ACL; Schema: genrep; Owner: postgres
--

REVOKE ALL ON TABLE tablarecurrente FROM PUBLIC;
REVOKE ALL ON TABLE tablarecurrente FROM postgres;
GRANT ALL ON TABLE tablarecurrente TO postgres;


--
-- TOC entry 2793 (class 0 OID 0)
-- Dependencies: 174
-- Name: tablarecurrente_evento; Type: ACL; Schema: genrep; Owner: postgres
--

REVOKE ALL ON TABLE tablarecurrente_evento FROM PUBLIC;
REVOKE ALL ON TABLE tablarecurrente_evento FROM postgres;
GRANT ALL ON TABLE tablarecurrente_evento TO postgres;


--
-- TOC entry 2794 (class 0 OID 0)
-- Dependencies: 163
-- Name: tablarecurrente_muestra; Type: ACL; Schema: genrep; Owner: postgres
--

REVOKE ALL ON TABLE tablarecurrente_muestra FROM PUBLIC;
REVOKE ALL ON TABLE tablarecurrente_muestra FROM postgres;
GRANT ALL ON TABLE tablarecurrente_muestra TO postgres;


-- Completed on 2018-09-27 11:59:30

--
-- PostgreSQL database dump complete
--
