<?php
include_once RUTA_PDO.'PDOSql.php';
/**
 * @access public
 * @copyright (c) 2016, Manejo Tenico de la Informacion
 * @since Abril 2016
 * 
 * 
 * @internal Las variables estan creadas segun el estandar CamelCase http://es.wikipedia.org/wiki/CamelCase
 * 
 * @author v1.0 Javier Garcia <javier.23.2010@gmail.com>
 */
class Configurador extends PDOSql{
    
    private $TablaRecurrente;
    private $TablaRecurrenteMuestra;
    private $TablaRecurrenteEvento;
    private $EventoJavascript;
    private $Informe;
    private $Cliente;
    private $BaseDeDatos;
    private $Generador;
    private $Duplicadas;
    
    /**
     * 
     * @param string $DB
     */
    public function __construct($DB) {
        
        include_once RUTA_REPORTEADOR.'clases/DOM/TablaRecurrente.php';
        include_once RUTA_REPORTEADOR.'clases/DOM/TablaRecurrenteMuestra.php';
        include_once RUTA_REPORTEADOR.'clases/DOM/TablaRecurrenteEvento.php';
        include_once RUTA_REPORTEADOR.'clases/DOM/EventoJavascript.php';
        include_once RUTA_REPORTEADOR.'clases/DOM/Informe.php';
        include_once RUTA_REPORTEADOR.'clases/DOM/Cliente.php';
        include_once RUTA_REPORTEADOR.'clases/DOM/BaseDeDatos.php';
        
        parent::__construct($DB);
        $this->TablaRecurrente = new TablaRecurrente($DB);
        $this->TablaRecurrenteMuestra = new TablaRecurrenteMuestra($DB);
        $this->TablaRecurrenteEvento = new TablaRecurrenteEvento($DB);
        $this->EventoJavascript = new EventoJavascript($DB);
        $this->Informe = new Informe($DB);
        $this->Cliente = new Cliente($DB);
        $this->BaseDeDatos = new BaseDeDatos($DB);
        $this->Duplicadas = 0;
    }
    
    /**
     * 
     * @return TablaRecurrente
     */
    public function getTablaRecurrente() {
        return $this->TablaRecurrente;
    }
    
    /**
     * 
     * @return TablaRecurrenteMuestra
     */
    public function getTablaRecurrenteMuestra() {
        return $this->TablaRecurrenteMuestra;
    }
    
    /**
     * 
     * @return TablaRecurrenteEvento
     */
    public function getTablaRecurrenteEvento() {
        return $this->TablaRecurrenteEvento;
    }
    
    /**
     * 
     * @return EventoJavascript
     */
    public function getEventoJavascript() {
        return $this->EventoJavascript;
    }
    
    /**
     * 
     * @return Informe
     */
    public function getInforme() {
        return $this->Informe;
    }
    
    /**
     * 
     * @return Cliente
     */
    public function getCliente() {
        return $this->Cliente;
    }
    
    /**
     * 
     * @return BaseDeDatos
     */
    public function getBaseDeDatos() {
        return $this->BaseDeDatos;
    }
        
    /**
     * 
     * @return Generador
     */
    public function getGenerador() {
        return $this->Generador;
    }
    
    /**
     * 
     * @return int
     */
    public function getDuplicadas() {
        return $this->Duplicadas;
    }

    /**
     * 
     */
    public function sumarDuplicadas() {
        $this->Duplicadas++;
    }
    
    /**
     * 
     */
    private function setGenerador() {
        
        include_once RUTA_REPORTEADOR.'clases/generador/Generador.php';
        $this->Generador = new Generador();
    }
    
    /**
     * 
     * @param array $Parametros
     * @return array
     */
    public function consultaTablaRecurrente($Parametros) {
        
        $Informe = array();
        $Tabla = $this->getTablaRecurrente()->consulta($Parametros['IdTabla'], $Parametros['Nivel'], $Parametros['IdPadre'], $Parametros['Informe'], $Parametros['Cliente']);
        if(count($Tabla) > 0){

            $this->setGenerador();
            foreach ($Tabla AS $DQ){
                
                $Informe[] = array(
                    "Sql" => $DQ['sql'],
                    "Campos" => $this->getGenerador()->consultaCampos($DQ['campos_muestra']),
                    "Descripcion" => $DQ['descripcion'],
                    "IdTabla" => $DQ['id_tablarecurrente'],
                    "IdPadre" => $DQ['id_tablarecurrente_padre'],
                    "TablaHija" => $DQ['id_tablarecurrente_hija'],
                    "IdBaseDatos" => $DQ['id_basededatos'],
                    "Nivel" => $DQ['nivel'],
                    "Eventos" => ($DQ['cantidad_eventos'] > 0) ? $this->getGenerador()->consultaEventos($DQ['id_tablarecurrente']) : array(),
                );
            }
        }
        return $Informe;
    }
    
    /**
     * 
     * @param array $Parametros
     * @return array
     */
    public function insertaTablaRecurrente($Parametros) {

        $Tabla = $this->getTablaRecurrente()->inserta($Parametros['Informe'], $Parametros['Nivel'], $Parametros['IdPadre']);
        
        return array(
            "Sql" => $Tabla[0]['sql'],
            "Campos" => array(),
            "Descripcion" => $Tabla[0]['descripcion'],
            "IdTabla" => $Tabla[0]['id_tablarecurrente'],
            "IdPadre" => $Tabla[0]['id_tablarecurrente_padre'],
            "TablaHija" => 0,
            "IdBaseDatos" => '',
            "Nivel" => $Tabla[0]['nivel'],
            "Eventos" => array(),
        );
    }
    
    /**
     * 
     * @param array $Limpio
     * @return array
     */
    public function gestionTablaCliente($Limpio) {
        
        if($Limpio['Hacer'] == 'crear'){
            
            $Cliente = $this->getCliente()->consuta($Limpio['NombreCliente']);
            if (count($Cliente) == 0) {
                
                $Creado = $this->getCliente()->inserta($Limpio['NombreCliente']);
                $Respuesta = array(
                    'Exitoso' => true,
                    'Mensaje' => 'Se creo el cliente ('.$Limpio['NombreCliente'].') exitosamente',
                    'IdCliente' => $Creado[0]['id_cliente']
                );
            }
            else{
                $Respuesta = array(
                    'Exitoso' => false,
                    'Mensaje' => 'El cliente ('.$Limpio['NombreCliente'].') ya existe.'
                );
            }
        }
        else if($Limpio['Hacer'] == 'editar'){
            
            $Cliente = $this->getCliente()->consuta(NULL, $Limpio['IdCliente']);
            if (count($Cliente) > 0) {
                
                $this->getCliente()->edita($Limpio['NombreCliente'], $Limpio['IdCliente']);
                $Respuesta = array(
                    'Exitoso' => true,
                    'Mensaje' => 'Se edito el cliente ('.$Cliente[0]['nombre'].') por ('.$Limpio['NombreCliente'].') exitosamente',
                    'IdCliente' => $Limpio['IdCliente']
                );
            }
            else{
                $Respuesta = array(
                    'Exitoso' => false,
                    'Mensaje' => 'El cliente ('.$Limpio['NombreCliente'].') no existe.'
                );
            }
        }
        $Respuesta = array_merge($Respuesta, array(
            'JsonClientes' => $this->crearArrayParaSelect(
                $this->getCliente()->consuta(), 
                'id_cliente',
                'nombre'
            )
        ));
        return $Respuesta;
    }
    
    /**
     * 
     * @param array $Limpio
     * @return array
     */
    public function gestionTablaInforme($Limpio) {
        
        if($Limpio['Hacer'] == 'crear'){
        
            $Informe = $this->getInforme()->consulta($Limpio['ClienteEnCR'], $Limpio['NombreInforme']);
            if(count($Informe) == 0){

                $Creado = $this->getInforme()->inserta($Limpio['ClienteEnCR'], $Limpio['NombreInforme']);
                $Respuesta = array(
                    'Exitoso' => true,
                    'IdInforme' => $Creado[0]['id_informe'],
                    'Mensaje' => 'Se creo el informe ('.$Creado[0]['nombre_informe'].') exitosamente con el ID '.$Creado[0]['id_informe']
                );
            }
            else{

                $Respuesta = array(
                    'Exitoso' => false,
                    'Mensaje' => 'El reporte ('.$Limpio['NombreInforme'].') ya existe'
                );
            }
        }
        elseif($Limpio['Hacer'] == 'editar') {

            $Informe = $this->getInforme()->consulta($Limpio['ClienteEnCR'], NULL, $Limpio['IdInforme']);
            if(count($Informe) > 0){

                $this->getInforme()->edita($Limpio['IdInforme'], $Limpio['NombreInforme']);
                $Respuesta = array(
                    'Exitoso' => true,
                    'IdInforme' => $Limpio['IdInforme'],
                    'Mensaje' => 'Se edito el informe ('.$Informe[0]['nombre_informe'].') por ('.$Limpio['NombreInforme'].') exitosamente'
                );
            }
            else{

                $Respuesta = array(
                    'Exitoso' => false,
                    'Mensaje' => 'El reporte ('.$Limpio['NombreInforme'].') no existe'
                );
            }
        }
        elseif($Limpio['Hacer'] == 'eliminar') {

            $Informe = $this->getInforme()->consulta($Limpio['ClienteEnCR'], NULL, $Limpio['IdInforme']);
            if(count($Informe) > 0){
                
                $this->getInforme()->elimina($Limpio['IdInforme']);
                $Respuesta = array(
                    'Exitoso' => true,
                    'IdInforme' => $Limpio['IdInforme'],
                    'Mensaje' => 'Se elimino el informe ('.$Informe[0]['nombre_informe'].') exitosamente'
                );
            }
            else{
                $Respuesta = array(
                    'Exitoso' => false,
                    'Mensaje' => 'El reporte ('.$Limpio['NombreInforme'].') no existe'
                );
            }
        }
        $Respuesta = array_merge($Respuesta, array(
           'Hacer' => $Limpio['Hacer'],
            'JsonInformes' => $this->crearArrayParaSelect(
                $this->getInforme()->consulta($Limpio['ClienteEnCR']), 
                'id_informe',
                'nombre_informe'
            )
        ));
        return $Respuesta;
    }
    
    /**
     * 
     * @param array $Limpio
     * @return array
     */
    public function gestionTablaBaseDeDatos($Limpio) {
        
        if($Limpio['Hacer'] == 'crear'){
            
            $Informe = $this->getBaseDeDatos()->consulta(NULL, strtoupper(trim($Limpio['Nombre'])));
            if(count($Informe) == 0){
                
                $Creado = $this->getBaseDeDatos()->inserta($Limpio['Nombre'], $Limpio['Usuario'], $Limpio['Clave'], $Limpio['DB'], $Limpio['Esquema'], $Limpio['Host'], $Limpio['Driver'], $Limpio['Puerto']);
                if(count($Creado) > 0){
                    $Respuesta = array(
                        'Exitoso' => true,
                        'IdBaseDatos' => $Creado[0]['id_basededatos'],
                        'Mensaje' => 'Se creo la base de datos ('.$Creado[0]['nombre'].') exitosamente'
                    );
                }
                else{
                    $Respuesta = array(
                        'Exitoso' => false,
                        'Mensaje' => 'Faltan datos obligatorios.'
                    );
                }
            }
            else{

                $Respuesta = array(
                    'Exitoso' => false,
                    'Mensaje' => 'La base de datos ('.$Limpio['Nombre'].') ya existe'
                );
            }
        }
        elseif($Limpio['Hacer'] == 'editar') {
            
            $Informe = $this->getBaseDeDatos()->consulta($Limpio['IdBaseDatos']);
            if(count($Informe) > 0){
                
                $Editado = $this->getBaseDeDatos()->edita($Limpio['Nombre'], $Limpio['Usuario'], $Limpio['Clave'], $Limpio['DB'], $Limpio['Esquema'], $Limpio['Host'], $Limpio['Driver'], $Limpio['Puerto'], $Limpio['IdBaseDatos']);
                if($Editado == true){
                    $Respuesta = array(
                        'Exitoso' => true,
                        'IdBaseDatos' => $Limpio['IdBaseDatos'],
                        'Mensaje' => 'Se edito la base de datos ('.$Limpio['Nombre'].') exitosamente'
                    );
                }
                else{
                    $Respuesta = array(
                        'Exitoso' => false,
                        'Mensaje' => 'Faltan datos obligatorios.'
                    );
                }
            }
            else{

                $Respuesta = array(
                    'Exitoso' => false,
                    'Mensaje' => 'El reporte ('.$Limpio['Nombre'].') no existe'
                );
            }
        }
        elseif($Limpio['Hacer'] == 'eliminar') {
            
            $Informe = $this->getBaseDeDatos()->consulta($Limpio['IdBaseDatos'], NULL, 'SI');
            if(count($Informe) > 0 and $Informe[0]['uso'] == 0){
                
                $Eliminado = $this->getBaseDeDatos()->elimina($Limpio['IdBaseDatos']);
                if($Eliminado == true){
                    $Respuesta = array(
                        'Exitoso' => true,
                        'IdBaseDatos' => $Limpio['IdBaseDatos'],
                        'Mensaje' => 'Se elimino la base de datos ('.$Informe[0]['nombre'].') exitosamente'
                    );
                }
                else{
                    $Respuesta = array(
                        'Exitoso' => false,
                        'Mensaje' => 'Faltan datos obligatorios.'
                    );
                }
            }
            elseif ($Informe[0]['uso'] > 0) {
                $Respuesta = array(
                    'Exitoso' => false,
                    'Mensaje' => 'La base de datos ('.$Limpio['Nombre'].') esta siendo utilizada en ('.$Informe[0]['uso'].') tablas'
                );
            }
            else{
                $Respuesta = array(
                    'Exitoso' => false,
                    'Mensaje' => 'La base de datos ('.$Limpio['Nombre'].') no existe'
                );
            }
        }
        $Respuesta = array_merge($Respuesta, 
            array(
                'Hacer' => $Limpio['Hacer'],
                'JsonBaseDatos' => $this->crearArrayParaSelect(
                    $this->getBaseDeDatos()->consulta(), 
                    'id_basededatos',
                    'nombre'
                )
            )
        );
        return $Respuesta;
    }
    
    /**
     * 
     * @param array $Parametros
     * @return boolean
     */
    public function diferenciaTablaRecurrente($Parametros) {
        
        $InformeVista = array(
            "Sql" => $Parametros['Sql'],
            "Campos" => ((isset($Parametros['NombreSqlaVista']) and count($Parametros['NombreSqlaVista']) > 0) ? $this->consultaCampos($Parametros['NombreSqlaVista'], $Parametros['NombreEnVista']) : array()),
            "Descripcion" => $Parametros['Descripcion'],
            "IdTabla" => $Parametros['IdTabla'],
            "IdPadre" => $Parametros['IdPadre'],
            "TablaHija" => $Parametros['TablaHija'],
            "IdBaseDatos" => $Parametros['IdBaseDatos'],
            "Nivel" => $Parametros['Nivel'],
            "Eventos" => ((isset($Parametros['NombreSqlaEvento']) and count($Parametros['NombreSqlaEvento']) > 0) ? 
            $this->consultaEventos($Parametros['NombreSqlaEvento'], $Parametros['NombreEvento'], $Parametros['DescripcionEvento'], $Parametros['TipoEvento'], $Parametros['IconoEvento'])
            : array())
        );
        $Parametros['Informe'] = NULL;
        $Parametros['Cliente'] = NULL;
        
        $InformeEnDB = $this->consultaTablaRecurrente($Parametros);
        $InformeDB = $InformeEnDB[0];

        $arrayDiff = array();
        foreach ($InformeVista as $Llave => $IV){

            if(is_array($InformeDB[$Llave]) and md5(var_export($InformeDB[$Llave], true)) != md5(var_export($IV, true))){
                $arrayDiff[$Llave] = $IV;
            }
            elseif($InformeDB[$Llave] != $IV){
                $arrayDiff[$Llave] = $IV;
            }
        }
        
        $Cambios = false;
        if (isset($arrayDiff['Sql']) or isset($arrayDiff['Descripcion']) or isset($arrayDiff['IdBaseDatos'])) {
            
            $this->getTablaRecurrente()->edita($Parametros['IdTabla'], $Parametros['Descripcion'], $Parametros['Sql'], $Parametros['IdBaseDatos']);
            $Cambios = true;
        }
        if(isset($arrayDiff['Campos'])){
            $this->getTablaRecurrenteMuestra()->eliminaSiNoEstanEn($Parametros['IdTabla'], array_keys($arrayDiff['Campos']));
            $Cambios = true;
        }
        $OrdenCampos = 0;
        foreach ($InformeVista['Campos'] as $EnSql => $EnVista) {

            $OrdenCampos++;
            $this->getTablaRecurrenteMuestra()->insertaOEdita($Parametros['IdTabla'], $EnSql, $EnVista, $OrdenCampos);
        }
        if(isset($arrayDiff['Eventos'])){
            
            $this->getTablaRecurrenteEvento()->eliminaSiNoEstanEn($Parametros['IdTabla'], $arrayDiff['Eventos']);
            if(count($arrayDiff['Eventos']) > 0){

                foreach ($arrayDiff['Eventos'] as $Evento) {

                    $this->getTablaRecurrenteEvento()->insertaOEdita(
                        $Parametros['IdTabla'],
                        $Evento['campo'],
                        $Evento['id_tipo'],
                        $Evento['descripcion'],
                        $Evento['evento'],
                        $Evento['icono']
                    );
                }
            }
            $Cambios = true;
        }
        return $Cambios;
    }
    
    /**
     * 
     * @param int $IdTabla
     * @param int $IdCliente
     * @param int $IdInforme
     * @return string
     */
    public function eliminarTablaRecurrente($IdTabla, $IdCliente, $IdInforme) {
        
        $Tabla = $this->getTablaRecurrente()->consulta($IdTabla, null, null, $IdInforme, $IdCliente);
        if(count($Tabla) > 0){
            
            $this->getTablaRecurrente()->elimina($IdTabla);
            return "Tabla eliminada exisosamente";
        }
        else{
            return "no se encontro ninguna tabla para los datos ingresados";
        }
    }
    
    /**
     * 
     * @param int $IdBaseDeDatos
     * @param string $Sql
     * @param array $Variables
     * @param int $Nivel
     * @return string|array
     */
    public function probarConsultaSql($IdBaseDeDatos, $Sql, $Variables, $Nivel) {
        
        $BaseDatos = $this->getBaseDeDatos()->consulta($IdBaseDeDatos);
        
        if(count($BaseDatos) > 0){
            
            $this->setGenerador();
            $Limpio = $Variables;
            $Respuesta = $this->getGenerador()->consultaTabla($Sql, $Limpio, $BaseDatos[0], $Nivel);
        }
        else{
            $Respuesta = "No se encontro la base de datos ingresada.";
        }
        return $Respuesta;
    }
    
    /**
     * 
     * @param array $Parametros
     * @return array
     */
    public function duplicarInforme($Parametros) {
        
        $Informe = $this->getInforme()->consulta($Parametros['Cliente'], $Parametros['NombreInforme']);
        if(count($Informe) == 0){

            $Creado = $this->getInforme()->inserta($Parametros['Cliente'], $Parametros['NombreInforme']);
            $Respuesta = array(
                'Exitoso' => true,
                'IdInforme' => $Creado[0]['id_informe'],
                'Mensaje' => 'Se duplico el informe ('.$Creado[0]['nombre_informe'].') exitosamente con el ID '.$Creado[0]['id_informe']
            );
            
            $this->duplicarTabla($Parametros['InformeCopiar'], 1, NULL, NULL,$Creado[0]['id_informe']);
        }
        else{

            $Respuesta = array(
                'Exitoso' => false,
                'Mensaje' => 'El reporte ('.$Parametros['NombreInforme'].') ya existe'
            );
        }
        return $Respuesta;
    }
    
    /**
     * 
     * @param int $IdInformeCopiar
     * @param int $NivelCopiar
     * @param int $IdPadreCopiar
     * @param int $IdTablaCopiar
     * @param int $IdInformePegar
     * @param int $IdPadrePegar
     * @param string $CopiarSubnivel Si|No
     */
    public function duplicarTabla($IdInformeCopiar = NULL, $NivelCopiar = NULL, $IdPadreCopiar = NULL, $IdTablaCopiar = NULL, $IdInformePegar = NULL, $IdPadrePegar = NULL, $CopiarSubnivel = 'Si') {
        
        $Tablas = $this->getTablaRecurrente()->consulta($IdTablaCopiar, $NivelCopiar, $IdPadreCopiar, $IdInformeCopiar, NULL);
        
        if(count($Tablas) > 0){
            foreach ($Tablas as $T) {

                $Duplicada = $this->getTablaRecurrente()->inserta(
                    $IdInformePegar, 
                    (($IdPadrePegar == NULL) ? 1 : NULL), 
                    $IdPadrePegar, 
                    $T['sql'], 
                    $T['descripcion'], 
                    $T['id_basededatos']
                );
                $this->getTablaRecurrenteMuestra()->duplica($T['id_tablarecurrente'], $Duplicada[0]['id_tablarecurrente']);
                $this->getTablaRecurrenteEvento()->duplica($T['id_tablarecurrente'], $Duplicada[0]['id_tablarecurrente']);
                $this->sumarDuplicadas();
                
                if($T['id_tablarecurrente_hija'] > 0 and $CopiarSubnivel == 'Si'){
                    $this->duplicarTabla(
                        NULL, 
                        ($T['nivel']+1), 
                        $T['id_tablarecurrente'], 
                        NULL, 
                        $IdInformePegar, 
                        $Duplicada[0]['id_tablarecurrente'], 
                        $CopiarSubnivel
                    );
                }
            }
        }
    }
    
    /**
     * 
     * @param int $IdInforme
     * @param int $IdCliente
     * @return array
     */
    public function estadoGeneralInforme($IdInforme, $IdCliente) {
        
        $Informe = $this->getInforme()->consulta($IdCliente, NULL, $IdInforme);
        $Mensaje = array();
        
        if(count($Informe) == 0){
            $Mensaje[] = "No se encontró ningún informe para los datos ingresados.";
        }
        $Problemas = $this->getInforme()->tablasConProblemas($IdInforme);
        if(count($Problemas) > 0){
            
            foreach ($Problemas as $P) {
                $Mensaje[] = "La tabla (".(($P['descripcion'] != '') ? $P['descripcion'] : "Sin descripción").") del nivel ".$P['nivel']." tiene los siguientes problemas: ";
                if($P['descripcion'] == '') $Mensaje[] = "No tiene descripción.";
                if($P['sql'] == '') $Mensaje[] = "No tiene Sql.";
                if($P['id_basededatos'] == '') $Mensaje[] = "No tiene base de datos asociada.";
                if($P['muestra'] == 0) $Mensaje[] = "No tiene campos a mostrar configurados.";
            }
        }
        return $Mensaje;
    }
    
    /**
     * 
     * @param array $NombreEnSql
     * @param array $NombreEnVista
     * @return array
     */
    private function consultaCampos($NombreEnSql, $NombreEnVista) {
        
        $Campos = array();
        foreach ($NombreEnSql as $Llave => $EnSql) {
            $Campos[$EnSql] = $NombreEnVista[$Llave];
        }
        return $Campos;
    }

    /**
     * 
     * @param array $NombreEnSql
     * @param array $NombreEvento
     * @param array $Descripcion
     * @param array $Tipo
     * @param array $Icono
     * @return array
     */
    private function consultaEventos($NombreEnSql, $NombreEvento, $Descripcion, $Tipo, $Icono) {
        
        $Eventos = array();
        foreach ($NombreEnSql as $Llave => $EnSql) {
            $Eventos[$EnSql] = array(
                'campo' => $EnSql,
                'descripcion' => $Descripcion[$Llave],
                'evento' => $NombreEvento[$Llave],
                'id_tipo' => $Tipo[$Llave],
                'icono' => $Icono[$Llave]
            );
        }
        return $Eventos;
    }

    /**
     * 
     * @param array $Arreglo
     * @param string $CampoLlave
     * @param string $CampoContenido
     * @return array
     */
    public function crearArrayParaSelect($Arreglo, $CampoLlave, $CampoContenido) {
        
        $Opciones = array();
        foreach ($Arreglo as $A) {
            $Opciones[] = array(
                'Llave' => $A[$CampoLlave],
                'Contenido' => $A[$CampoContenido]
            );
        }
        return $Opciones;
    }
}
