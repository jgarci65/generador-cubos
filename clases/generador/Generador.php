<?php
include_once RUTA_PDO.'PDOSql.php';
/**
 * @access public
 * @since Mayo 2018
 * 
 * 
 * @internal Las variables estan creadas segun el estandar CamelCase http://es.wikipedia.org/wiki/CamelCase
 * 
 * @author v1.0 Javier Garcia <javier.23.2010@gmail.com>
 */
class Generador{
    
    private $Conexiones = array();
    private $TablaRecurrente;
    private $EventoJavascript;
    private $BaseDeDatos;
    
    public function __construct() {
        
        include_once RUTA_REPORTEADOR.'config/path.php';
        include_once RUTA_REPORTEADOR.'clases/DOM/TablaRecurrente.php';
        include_once RUTA_REPORTEADOR.'clases/DOM/EventoJavascript.php';
        include_once RUTA_REPORTEADOR.'clases/DOM/BaseDeDatos.php';
        
        $this->setConexion("generador", CONECTAR_A);
        $this->TablaRecurrente = new TablaRecurrente(CONECTAR_A);
        $this->EventoJavascript = new EventoJavascript(CONECTAR_A);
        $this->BaseDeDatos = new BaseDeDatos(CONECTAR_A);
    }
    
    /**
     * 
     * @return TablaRecurrente
     */
    private function getTablaRecurrente() {
        return $this->TablaRecurrente;
    }
    
    /**
     * 
     * @return EventoJavascript
     */
    private function getEventoJavascript() {
        return $this->EventoJavascript;
    }
    
    /**
     * 
     * @param string $NombreDB
     * @return PDOSql
     */
    public function getConexion($NombreDB) {
        return isset($this->Conexiones[$NombreDB]) ? $this->Conexiones[$NombreDB] : false;
    }
    
    /**
     * 
     * @param string $NombreDB
     * @param string $DB
     */
    private function setConexion($NombreDB, $DB) {
        
        include_once RUTA_PDO.'PDOSql.php';
        $this->Conexiones[$NombreDB] = new PDOSql($DB);
    }

    /**
     *
     * @return BaseDeDatos
     */
    private function getBaseDeDatos() {
        return $this->BaseDeDatos;
    }
    
    /**
     * 
     * @param string $Sql
     * @param array $Variables
     * @param array $DatosConexion
     * @param int $Nivel
     * @return array
     */
    public function consultaTabla($Sql, $Variables, $DatosConexion, $Nivel) {

        $this->setConexion($DatosConexion['base_de_datos'], $DatosConexion);
        if(count($Variables) > 0){
            
            # if($Nivel == 1){
                $i = 0;
                foreach ($Variables as $Variable => $Contenido) {
                    
                    $i++;
                    if(trim($Contenido) == ""){
                        $Sql = $this->quitarFiltro($Sql, $Variable);
                    }
                    else{
                        $Sql = str_replace("{".$Variable."}", $Contenido, $Sql);
                    }
                }
            /*}
            else{
                
                foreach ($Variables as $Variable => $Contenido) {
                    
                    $Sql = str_replace("{".$Variable."}", $Contenido, $Sql);
                }
            }*/
            $Sql = str_replace("WHERE AND", "WHERE", $Sql);
        }
        
        try {
            # echo "<pre>$Sql</pre>";
            return $this->getConexion($DatosConexion['base_de_datos'])->pasarelaSql($Sql, 'assoc');
        } catch (Exception $Ex) {
            
            echo $Ex->getMessage()."
            $Sql
            ";
        }
        
    }
    
    /**
     * 
     * @param string $Sql
     * @param string $Variable
     * @return string
     */
    private function quitarFiltro($Sql, $Variable) {
        
        $Posicion = strpos($Sql, "{".$Variable."}");
        if($Posicion > 0){
            
            $SqlAntesPosicion = substr($Sql, 0, $Posicion);
        
            $SqlDespuesPosicion = str_replace("{".$Variable."}", "", substr($Sql, $Posicion));
            $SqlDespuesSinSaltos = str_replace("\r\n", " ", $SqlDespuesPosicion);
            $ArrDespues = explode(" ", $SqlDespuesSinSaltos);
            $DondeANDDespues = array_search("AND", $ArrDespues);

            if($DondeANDDespues > -1){

                for($i = 0;$i < $DondeANDDespues; $i++){
                    array_shift($ArrDespues);
                }
                $SqlDespuesPosicion = implode(" ", $ArrDespues);
            }
            else{
                $SqlDespuesPosicion = "";
            }
            
            $SqlAntesSinEspacios = str_replace("\r\n", " ", $SqlAntesPosicion);
            $Arr = explode(" ", $SqlAntesSinEspacios);
            $ArrReverso = array_reverse($Arr);
            $DondeAND = array_search("AND", $ArrReverso);
            $DondeWHERE = array_search("WHERE", $ArrReverso);
            
            if($DondeAND < $DondeWHERE){
                $Donde = $DondeAND;
            }
            else{
                $Donde = ($DondeWHERE-1);
            }
            
            for($i = 0;$i <= $Donde; $i++){
                array_shift($ArrReverso);
            }
            if($ArrReverso[0] == "AND"){
                array_shift($ArrReverso);
            }
            $ArrSinReverso = array_reverse($ArrReverso);
            return implode(" ", $ArrSinReverso)." ".$SqlDespuesPosicion;
        }
        else{
            return $Sql;
        }
    }
    
    /**
     * 
     * @param string $Campos
     * @return array
     */
    public function consultaCampos($Campos) {
        
        $LlaveCampo = array();
        if($Campos != "" and strpos($Campos, ",") != FALSE){
            
            $ArregloCampos = explode(",", $Campos);
            foreach ($ArregloCampos as $AC) {
                
                if(strpos($AC, "=") != FALSE){
                    $Relacion = explode("=", $AC);
                    $LlaveCampo[trim($Relacion[0])] = trim($Relacion[1]);
                }
                else{
                    $LlaveCampo[trim($AC)] = trim($AC);
                }
            }
        }
        elseif($Campos != ""){
            if(strpos($Campos, "=") != FALSE){
                $Relacion = explode("=", $Campos);
                $LlaveCampo[trim($Relacion[0])] = trim($Relacion[1]);
            }
            else{
                $LlaveCampo[trim($Campos)] = trim($Campos);
            }
        }
        return $LlaveCampo;
    }
    
    /**
     * 
     * @param int $IdTabla
     * @return array
     */
    public function consultaEventos($IdTabla) {
        
        $Eventos = $this->getEventoJavascript()->consultaEventosTabla($IdTabla);
        $CnfEventos = array();
        if(count($Eventos) > 0){
            foreach ($Eventos AS $E){
                $CnfEventos[$E['campo']] = $E;
            }
        }
        return $CnfEventos;
    }

    /**
     * 
     * @param int $IdCliente
     * @param int $IdInforme
     * @param int $Nivel
     * @param int $IdTablaPadre
     * @param string $Variables
     * @return array
     */
    public function generarInforme($IdCliente, $IdInforme, $Nivel, $IdTablaPadre, $Variables) {
        
        $DatosQuery = $this->getTablaRecurrente()->consulta(NULL, $Nivel, $IdTablaPadre, $IdInforme, $IdCliente);
        
        $Informe = array();
        if(count($DatosQuery) > 0){
            
            foreach ($DatosQuery AS $DQ){

                $DConexion = $this->getBaseDeDatos()->consulta($DQ["id_basededatos"]);
                
                $Informe[] = array(
                    "Tabla" => $this->consultaTabla($DQ['sql'], $Variables, $DConexion[0], $DQ['nivel']),
                    "Campos"=> $this->consultaCampos($DQ['campos_muestra']),
                    "Descripcion" => $DQ['descripcion'],
                    "IdTabla" => $DQ['id_tablarecurrente'],
                    "IdTablaPadre" => $DQ['id_tablarecurrente_padre'],
                    "TablaHija" => $DQ['id_tablarecurrente_hija'],
                    "Nivel" => $DQ['nivel'],
                    "Eventos" => ($DQ['cantidad_eventos'] > 0) ? $this->consultaEventos($DQ['id_tablarecurrente']) : array(),
                    "OpcionesTabla" => array(
                        "foot" => false
                    )
                );
            }
        }
        return $Informe;
    }
}
