<?php

/**
 * @access public
 * @copyright (c) 2014, Manejo Tenico de la Informacion
 * @since Octubre 2015
 *
 * @version 1.0
 *
 * @uses Clase encargada de manera la vista de la aplicación
 *
 * @author Javier Garcia <javier.23.2010@gmail.com>
 */
class View{

    private $URL;
    private $HTML_MENU;

    public function __construct(){

        $this->URL = $this->get_url_raiz();
    }

    /**
     *
     * @return html
     */
    public function getHTML_MENU() {
        return $this->HTML_MENU;
    }

    /**
     *
     * @param html $HTML_MENU
     */
    function setHTML_MENU($HTML_MENU) {
        $this->HTML_MENU = $HTML_MENU;
    }

    /**
     *
     * @uses - Obtiene el nivel de directorio en que esta ubicada la pagina
     * @copyright http://www.desarrolloweb.com/articulos/1188.php
     *
     * @return string
     */
    private function get_url_raiz(){

        # 1. Se hace un recuento de los caracteres que contiene el nombre del script actual.
        $Chars = count_chars($_SERVER['PHP_SELF'],1);

        # 2. Exploramos la tabla de los caracteres devueltos buscando el car�cter ('/' C�digo 47 ) de directorio (carpeta) que devuelve Apache.
        foreach ($Chars as $Char=>$nChars){
            if ($Char==47) {$n=$nChars;break;}
        }

        # 3. Generamos una cadena de n-1 veces con la subcandena "../" que nos da el nivel de directorio en que se encuentra el script. Para este caso 2
        if ($n==0) $PathString=""; else $PathString=str_pad("",($n-2)*3,"../");
        return $PathString;
    }

    /**
     *
     * @return string
     */
    public function get_url(){

        return $this->URL;
    }

    /**
     * @access public
     * @example Metodo para consulta las encuestas
     *
     * @param $ar_EXTENDS: Datos de configuracion a utilizar en el metodo
     *
     *
     * @return Layout con apariencia de la pagina
     *
     * @author Javier Garcia
     * @copyright Tu Software S.A.S.
     */
    public function get_layout($ar_EXTENDS = array()){

        $ar_DEFAULT = array(
            'DOCTYPE' => '<!DOCTYPE html>',
            'TITLE' => 'Diseño adaptable',
            'METADATA' => array(
                "CHARSET" => '<meta charset="utf-8">',
                "X-FRAME" => '<meta http-equiv="X-Frame-Options" content="deny">',
                "CACHE" => '<meta http-equiv="cache-control" content="no-cache, no-store, must-revalidate, private">',
                "PRAGMA" => '<meta http-equiv="pragma" content="no-cache" />',
                "AUTOR1" => '<meta name="author" content="Aplicativo - Javier Garcia Ruiz <javier.23.2010@gmail.com>">',
            ),
            'CSS' => array(
                "BOOTSTRAP-MIN" => '<link href="'.$this->get_url().'css/bootstrap.min.css" rel="stylesheet">',
                "NAVBAR" => '<link href="'.$this->get_url().'css/navbar.css" rel="stylesheet">',
                # "BOOTSTRAP-THEME" => '<link href="'.$this->get_url().'css/bootstrap-theme.css" rel="stylesheet">',
                "GRID" => '<link href="'.$this->get_url().'css/grid.css" rel="stylesheet">',
                "TEMA" => '<link href="'.$this->get_url().'css/navbar-rojo.css" rel="stylesheet">',
                "FOOTER" => '<link href="'.$this->get_url().'css/sticky-footer.css" rel="stylesheet">'
            ),
            'JAVASCRIPT' => array(

            ),
            'JAVASCRIPT-PAGE' => array(),
            'JAVASCRIPT-FOOTER' => array(
                "JQUERY" => '<script src="'.$this->get_url().'js/jquery-1.11.2.min.js"></script>',
                "BOOTSTRAP" => '<script src="'.$this->get_url().'js/bootstrap.min.js"></script>',
            ),
            'LAYOUT' => "layout/layout.html",
            'HEADER' => $this->set_contenido_vista(array( "MENU" => $this->getHTML_MENU()), "layout/header.html"),
            # 'MENU' => $this->getHTML_MENU(),
            'NAVIGATION' => array(),
            'CONTENT' => 'Contenido HTML Pagina',
            'FOOTER' => '&reg; Todos los derechos reservados. MTI '.date("Y")
        );

        $ar_USAR = $this->array_merge_multidimensional($ar_DEFAULT, $ar_EXTENDS);
        $vhtml_LAYOUT = $this->set_contenido_vista($ar_USAR,$ar_USAR['LAYOUT']);
        return $vhtml_LAYOUT;
    }

    /**
     *
     * @param array $ar_DEFAULT
     * @param array $ar_EXTENDS
     * @return array
     */
    private function array_merge_multidimensional($ar_DEFAULT, $ar_EXTENDS){

        foreach($ar_DEFAULT AS $K => $C){
            if(isset($ar_EXTENDS[$K])){

                if(is_array($ar_DEFAULT[$K])){
                    $ar_USAR[$K] = array_merge($ar_DEFAULT[$K],$ar_EXTENDS[$K]);
                }
                else if($ar_EXTENDS[$K] == ''){
                    $ar_USAR[$K] = $ar_DEFAULT[$K];
                }
                else{
                    $ar_USAR[$K] = $ar_EXTENDS[$K];
                }
            }
            else{
                $ar_USAR[$K] = $ar_DEFAULT[$K];
            }
        }
        return $ar_USAR;
    }

    /**
     *
     */
    public function construirHtmlMenu(){

        # array_column esta disponible nativamente desde php > 5.5 debido a que se trabaja con uan version anterior, se ingresa como plugin
        require_once $this->URL.'plugin/array_column.php';

        unset($_SESSION['ENTORNOG']['MENU']['null']);
        $MODULOS = array_unique(array_column($_SESSION['ENTORNOG']['MENU'],'modulo'));

        $HTML = '';
        foreach ($MODULOS AS $M){

            if($M != 'null'){
                $APLICACIONES = array_filter($_SESSION['ENTORNOG']['MENU'], function($var) use($M) {
                    # Filtra las aplicaciones de cada modulo
                    return in_array($M, $var) ? true : false;
                });
                $Itera = 1;

                foreach ($APLICACIONES AS $APS){

                    if($Itera == 1){
                        $HTML .= '<li class="dropdown">';
                        $HTML .= '<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">'.$APS['modulo'].' <span class="caret"></span></a>';
                        $HTML .= '<ul class="dropdown-menu" role="menu">';
                    }
                    $HTML .= '<li><a href="'.$this->get_url().$APS['name_action'].'">'.$APS['descripcion'].'</a></li>';

                    if(count($APLICACIONES) == $Itera){
                        $HTML .= '</ul>' .
                            '</li>';
                    }
                    $Itera++;
                }
            }
        }
        $this->setHTML_MENU($HTML);
    }

    /**
     * @access public
     *
     * @param ac_DICCIONARIO: Datos que relacionan los elementos a remplazar en la vista
     * @param vs_TEMPLATE: Ruta del template a utilizar
     *
     * @return array
     *
     * @author Javier Garcia
     */
    public function set_contenido_vista($ac_DICCIONARIO_VIEW,$vhtml_TEMPLATE){

        $vhtml_CONTENT = file_get_contents($this->URL.$vhtml_TEMPLATE);
        foreach($ac_DICCIONARIO_VIEW as $clave => $valor){
            if(is_array($valor)) $valor = implode(" ",$valor);

            $vhtml_CONTENT = str_replace("{".$clave."}",$valor,$vhtml_CONTENT);
        }
        return $vhtml_CONTENT;
    }
}