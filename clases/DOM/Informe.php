<?php
include_once RUTA_PDO.'PDOSql.php';
/**
 * Description of Informe
 *
 * @author javierg.garcia
 */
class Informe extends PDOSql{
    
    public function __construct($DB) {
        
        parent::__construct($DB);
    }
    
    /**
     * 
     * @param int $IdCliente
     * @param string $Nombre
     * @param int $Id
     * @return array
     */
    public function consulta($IdCliente = NULL, $Nombre = NULL, $Id = NULL) {
        
        $FiltroSql = array();
        if ($IdCliente != NULL) $FiltroSql[] = "id_cliente = ".$IdCliente;
        if ($Nombre != NULL) $FiltroSql[] = "nombre_informe = '".$Nombre."'";
        if ($Id != NULL) $FiltroSql[] = "id_informe = ".$Id;
        
        $InformeSql = "
        SELECT id_informe, nombre_informe, id_cliente
        FROM ".ESQUEMA."informe_cliente
        ".((count($FiltroSql) > 0) ? "WHERE ".implode(" AND ", $FiltroSql) : "")."
        ";
        return $this->pasarelaSql($InformeSql, 'assoc');
    }
    
    /**
     * 
     * @param int $Id
     * @return array
     */
    public function tablasConProblemas($Id = NULL) {
        
        $EstadoSql = "
        SELECT *
        FROM (
            SELECT (
                SELECT COUNT(*) FROM ".ESQUEMA."tablarecurrente_muestra trm WHERE trm.id_tablarecurrente = tr.id_tablarecurrente
            ) AS muestra,
            (
                SELECT COUNT(*) FROM ".ESQUEMA."tablarecurrente_evento tre WHERE tre.id_tablarecurrente = tr.id_tablarecurrente
            ) AS eventos,
            sql, descripcion, id_basededatos, id_informe, nivel
            FROM ".ESQUEMA."tablarecurrente tr
            WHERE id_informe = ".$Id."
        ) AS t
        WHERE sql = ''
        OR descripcion = ''
        OR muestra = 0
        ";
        return $this->pasarelaSql($EstadoSql, 'assoc');
    }
    
    /**
     * 
     * @param int $IdCliente
     * @param string $Nombre
     * @return array
     */
    public function inserta($IdCliente, $Nombre) {

        if($IdCliente > 0 and $Nombre != ""){

            $InformeSql = "
            INSERT INTO ".ESQUEMA."informe_cliente (
                nombre_informe,
                id_cliente
            )
            VALUES (
                '".  strtoupper(trim($Nombre))."',
                ".$IdCliente."
            )
            RETURNING id_informe, nombre_informe
            ";
            return $this->exec($InformeSql, 'assoc');
        }
        else{
            return false;
        }
    }
    
    /**
     * 
     * @param int $Id
     * @param string $Nombre
     * @return bool
     */
    public function edita($Id, $Nombre) {

        if($Id > 0 and $Nombre != ""){

            $InformeSql = "
            UPDATE ".ESQUEMA."informe_cliente 
                SET nombre_informe = '".trim(strtoupper($Nombre))."'
            WHERE id_informe = ".$Id."
            ";
            $this->exec($InformeSql);
            return true;
        }
        else{
            return false;
        }
    }
    
    /**
     * 
     * @param int $Id
     * @return bool
     */
    public function elimina($Id) {

        if($Id > 0){

            $InformeSql = "
            DELETE FROM ".ESQUEMA."informe_cliente
            WHERE id_informe = ".$Id."
            ";
            $this->exec($InformeSql);
            return true;
        }
        else{
            return false;
        }
    }
}
