<?php
include_once RUTA_PDO.'PDOSql.php';
/**
 * @access public
 * @since Mayo 2018
 *
 *
 * @internal Las variables estan creadas segun el estandar CamelCase http://es.wikipedia.org/wiki/CamelCase
 *
 * @author v1.0 Javier Garcia <javier.23.2010@gmail.com>
 */
class TablaRecurrente extends PDOSql{
    
    /**
     * 
     * @param string $DB
     */
    public function __construct($DB) {
        
        parent::__construct($DB);
    }
    
    /**
     * 
     * @param int $Id
     * @param int $Nivel
     * @param int $IdPadre
     * @param int $IdInforme
     * @param int $IdCliente
     * @return array
     */
    public function consulta($Id = NULL, $Nivel = NULL, $IdPadre = NULL, $IdInforme = NULL, $IdCliente = NULL) {
        
        $FiltroSql = array();
        if ($Id != NULL) $FiltroSql[] = "tr.id_tablarecurrente = ".$Id;
        if ($Nivel != NULL) $FiltroSql[] = "tr.nivel = ".$Nivel;
        if ($IdPadre != NULL) $FiltroSql[] = "tr.id_tablarecurrente_padre = ".$IdPadre."".(($Nivel > 1) ? " AND tr.id_tablarecurrente_padre <> tr.id_tablarecurrente" : "");
        if ($IdInforme != NULL) $FiltroSql[] = "ic.id_informe = ".$IdInforme;
        if ($IdCliente != NULL) $FiltroSql[] = "ic.id_cliente = ".$IdCliente;
        
        $ConsultaSql = "
        SELECT tr.id_tablarecurrente, tr.sql, tr.nivel, tr.descripcion, tr.id_tablarecurrente_padre, tr.id_informe, tr.id_basededatos, 
        bdd.base_de_datos AS nombredb, 
        ARRAY_TO_STRING(
            ARRAY(
                SELECT campo_query||' = '||campo_vista
                FROM ".ESQUEMA."tablarecurrente_muestra trm
                WHERE trm.id_tablarecurrente = tr.id_tablarecurrente
                ORDER BY orden ASC, campo_vista ASC
            )
            , ','
        ) AS campos_muestra,
        (
            SELECT COUNT(*)
            FROM ".ESQUEMA."tablarecurrente_evento tre
            WHERE tre.id_tablarecurrente = tr.id_tablarecurrente
        ) AS cantidad_eventos,
        (
            SELECT COUNT(*)
            FROM ".ESQUEMA."tablarecurrente str 
            WHERE str.id_tablarecurrente_padre = tr.id_tablarecurrente
                AND str.nivel = (tr.nivel + 1)
        ) AS id_tablarecurrente_hija, bdd.id_basededatos
        FROM ".ESQUEMA."tablarecurrente tr
            LEFT JOIN ".ESQUEMA."basededatos bdd ON tr.id_basededatos = bdd.id_basededatos
            INNER JOIN ".ESQUEMA."informe_cliente ic ON tr.id_informe = ic.id_informe
        ".((count($FiltroSql) > 0) ? "WHERE ".implode(" AND ", $FiltroSql) : "")."
        ";
        return $this->pasarelaSql($ConsultaSql, 'assoc');
    }
    
    /**
     * 
     * @param int $IdInforme
     * @param int $IdCliente
     * @return array
     */
    public function consultaPorNiveles($IdInforme = NULL, $IdCliente = NULL) {
        
        $FiltroSql = array();
        if ($IdInforme != NULL) $FiltroSql[] = "ic.id_informe = ".$IdInforme;
        if ($IdCliente != NULL) $FiltroSql[] = "ic.id_cliente = ".$IdCliente;
        
        $ConsultaSql = "
        SELECT MAX(nivel) AS nivel
        FROM ".ESQUEMA."tablarecurrente tr
            INNER JOIN ".ESQUEMA."informe_cliente ic ON tr.id_informe = ic.id_informe
        ".((count($FiltroSql) > 0) ? "WHERE ".implode(" AND ", $FiltroSql) : "")."
        LIMIT 1
        ";
        $Nivel = $this->pasarelaSql($ConsultaSql, 'assoc');
        
        $Detalle = array();
        if(count($Nivel) > 0){
            
            for ($i = 1; $i <= $Nivel[0]['nivel']; $i++){
                $Detalle[$i] = $this->consulta(NULL, $i, NULL, $IdInforme, $IdCliente);
            }
        }
        return $Detalle;
    }
    
    /**
     * 
     * @param string $Sql
     * @return array
     */
    public function consultaVariablesSql($Sql) {
        
        $Tomar = false;
        $Variable = '';
        $Variables = array();
        $v = 0;
        
        for($i=0; $i<strlen($Sql); $i++){
            
            if($Sql[$i] == '{'){
                $Tomar = true;
            }
            elseif ($Sql[$i] == '}') {
                $Tomar = false;
                $Variables[$v] = $Variable;
                $v++;
                $Variable = '';
            }
            if($Tomar == true and $Sql[$i] != '{'){
                $Variable .= $Sql[$i];
            }
        }
        return array_unique($Variables, SORT_STRING);
    }
    
    /**
     * 
     * @param int $IdInforme
     * @param int $Nivel
     * @param int $IdPadre
     * @param string $Sql
     * @param string $Descripcion
     * @param int $IdBaseDeDatos
     * @return array
     */
    public function inserta($IdInforme, $Nivel = 1, $IdPadre = NULL, $Sql = "", $Descripcion = "", $IdBaseDeDatos = NULL) {

        $InsertaSql = "
        INSERT INTO ".ESQUEMA."tablarecurrente (
            descripcion,
            sql,
            nivel,
            id_tablarecurrente_padre,
            id_informe,
            id_basededatos
        )
        VALUES (
            '".trim($Descripcion)."',
            '".pg_escape_string(trim($Sql))."',
            ".(($IdPadre != NULL) ? "(SELECT nivel+1 FROM ".ESQUEMA."tablarecurrente WHERE id_tablarecurrente = ".$IdPadre." LIMIT 1)," : $Nivel.",")."
            ".(($IdPadre != NULL) ? $IdPadre."," : "NULL,")."
            ".$IdInforme."
            ".(($IdBaseDeDatos != NULL) ? ",".$IdBaseDeDatos : ", NULL")."
        )
        RETURNING *
        ";
        return $this->exec($InsertaSql, 'assoc');
    }
    
    /**
     * 
     * @param int $Id
     * @param string $Descripcion
     * @param string $Sql
     * @param int $IdBaseDatos
     * @return bool
     */
    public function edita($Id, $Descripcion = NULL, $Sql = NULL, $IdBaseDatos = NULL) {
        
        $SetSql = array();
        if ($Descripcion != NULL) $SetSql[] = "descripcion= '".trim($Descripcion)."'";
        if ($Sql != NULL) $SetSql[] = "sql = '".pg_escape_string($Sql)."'";
        if ($IdBaseDatos != NULL) $SetSql[] = "id_basededatos = ".$IdBaseDatos;
        
        if ($Id > 0 and count($SetSql) > 0) {
            
            $EditaSql = "
            UPDATE ".ESQUEMA."tablarecurrente
            SET ".implode(", ", $SetSql)."
            WHERE id_tablarecurrente = ".$Id."
            ";
            $this->exec($EditaSql);
            return true;
        }
        else{
            return false;
        }
    }
    
    /**
     * 
     * @param int $Id
     */
    public function elimina($Id) {

        if($Id > 0){

            $EliminaSql = "
            DELETE FROM ".ESQUEMA."tablarecurrente
            WHERE id_tablarecurrente = ".$Id."
            ";
            $this->exec($EliminaSql);
            return true;
        }
        else{
            return false;
        }
    }
}
