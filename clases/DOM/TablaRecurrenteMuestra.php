<?php
include_once RUTA_PDO.'PDOSql.php';
/**
 * Description of TablaRecurrenteMuestra
 *
 * @author Javier Garcia Ruiz <javier.23.2010@gmail.com>
 */
class TablaRecurrenteMuestra extends PDOSql{
    
    /**
     * 
     * @param string $DB
     */
    public function __construct($DB) {
        
        parent::__construct($DB);
    }
    
    /**
     * 
     * @param int $IdTabla
     * @param string $NombreEnSql
     * @param string $NombreEnVista
     * @return array
     */
    public function consulta($IdTabla = NULL, $NombreEnSql = NULL, $NombreEnVista = NULL) {
        
        $FiltroSql = array();
        if ($IdTabla != NULL) $FiltroSql[] = "id_tablarecurrente = ".$IdTabla;
        if ($NombreEnSql != NULL) $FiltroSql[] = "campo_query = '".trim(strtolower($NombreEnSql))."'";
        if ($NombreEnVista != NULL) $FiltroSql[] = "campo_vista = '".trim(strtolower($NombreEnVista))."'";
        
        $ConsultaSql = "
        SELECT id_tablarecurrente, campo_query AS campoensql, campo_vista AS compoenvista, orden
        FROM ".ESQUEMA."tablarecurrente_muestra
        ".((count($FiltroSql) > 0) ? "WHERE ".implode(" AND ", $FiltroSql) : "")."
        ";
        return $this->pasarelaSql($ConsultaSql, 'assoc');
    }
    
    /**
     * 
     * @param int $IdTabla
     * @param string $NombreEnSql
     * @param string $NombreEnVista
     * @param int $Orden
     */
    public function insertaOEdita($IdTabla, $NombreEnSql, $NombreEnVista, $Orden) {
        
        $UpdateSql = "
        UPDATE ".ESQUEMA."tablarecurrente_muestra
        SET campo_vista = '".$NombreEnVista."', orden = ".$Orden."
        WHERE id_tablarecurrente = ".$IdTabla."
            AND campo_query = '".trim(strtolower($NombreEnSql))."';";
        $this->exec($UpdateSql);
        
        $InsertSql = "INSERT INTO ".ESQUEMA."tablarecurrente_muestra (
            id_tablarecurrente, campo_query, campo_vista, orden
        )
        SELECT ".$IdTabla.", 
            '".trim(strtolower($NombreEnSql))."',
            '".trim($NombreEnVista)."',
            ".$Orden."
        WHERE NOT EXISTS (
            SELECT 1 FROM ".ESQUEMA."tablarecurrente_muestra
            WHERE id_tablarecurrente = ".$IdTabla."
                AND campo_query = '".trim(strtolower($NombreEnSql))."'
        );
        ";
        $this->exec($InsertSql);
    }
    
    /**
     * 
     * @param int $IdTabla
     * @param string $NombreEnSql
     */
    public function elimina($IdTabla, $NombreEnSql) {

        if($IdTabla > 0 and $NombreEnSql != ""){

            $EliminaSql = "
            DELETE FROM ".ESQUEMA."tablarecurrente_muestra
            WHERE id_tablarecurrente = ".$IdTabla."
                AND campo_query = '".  trim(strtolower($NombreEnSql))."'
            RETURNING id_tablarecurrente
            ";
            $Eliminados = $this->exec($EliminaSql, 'assoc');
            if(count($Eliminados) > 0){
                $this->reOrdenarCampos($IdTabla);
            }
            return true;
        }
        else{
            return false;
        }
    }
    
    /**
     * 
     * @param int $IdTabla
     * @param array|string $NombreEnSql
     */
    public function eliminaSiNoEstanEn($IdTabla, $NombreEnSql) {
        
        $EliminaSql = "
        DELETE FROM ".ESQUEMA."tablarecurrente_muestra
        WHERE id_tablarecurrente = ".$IdTabla."
            AND campo_query ".((is_array($NombreEnSql)) ? "NOT IN ('".implode("','",$NombreEnSql)."')" : "!= '".trim(strtolower($NombreEnSql))."'")."
        ";
        $Eliminados = $this->exec($EliminaSql);
        if(count($Eliminados) > 0){
            $this->reOrdenarCampos($IdTabla);
        }
    }
    
    /**
     * 
     * @param int $IdTabla
     */
    private function reOrdenarCampos($IdTabla) {
        
        $ConsultaSql = "
        SELECT ".ESQUEMA."fn_reordenar_trm(".$IdTabla.")
        ";
        $this->pasarelaSql($ConsultaSql);
    }
    
    /**
     * 
     * @param int $IdTablaCopia
     * @param int $IdTablaPega
     */
    public function duplica($IdTablaCopia, $IdTablaPega) {

        if($IdTablaCopia > 0 and $IdTablaPega > 0){

            $DuplicaSql = "
            INSERT INTO ".ESQUEMA."tablarecurrente_muestra (
                id_tablarecurrente,
                campo_query,
                campo_vista,
                orden
            )
            SELECT ".$IdTablaPega.", campo_query, campo_vista, orden
            FROM ".ESQUEMA."tablarecurrente_muestra
            WHERE id_tablarecurrente = ".$IdTablaCopia."
            ";
            $this->exec($DuplicaSql);
            return true;
        }
        else{
            return false;
        }
    }
}
