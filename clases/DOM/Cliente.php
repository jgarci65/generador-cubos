<?php
include_once RUTA_PDO.'PDOSql.php';
/**
 * Description of Cliente
 *
 * @author javierg.garcia
 */
class Cliente extends PDOSql{
    
    public function __construct($DB) {
        
        parent::__construct($DB);
    }
    
    /**
     * 
     * @param string $Nombre
     * @param int $Id
     * @return array
     */
    public function consuta($Nombre = NULL, $Id = NULL) {
        
        $FiltroSql = array();
        if($Id != NULL) $FiltroSql[] = "id_cliente = '".$Id."'";
        if($Nombre != NULL) $FiltroSql[] = "nombre = '".trim(strtoupper($Nombre))."'";
        
        $ClienteSql = "
        SELECT id_cliente, nombre
        FROM ".ESQUEMA."cliente
        ".((count($FiltroSql) > 0) ? "WHERE ".implode(" AND ", $FiltroSql) : "")."
        ORDER BY nombre ASC
        ";
        return $this->pasarelaSql($ClienteSql, 'assoc');
    }
    
    /**
     * 
     * @param string $Nombre
     * @return array
     */
    public function inserta($Nombre) {

        if($Nombre != ""){

            $ClienteSql = "
            INSERT INTO ".ESQUEMA."cliente (
                nombre
            )
            VALUES (
                '".trim(strtoupper($Nombre))."'
            )
            RETURNING id_cliente, nombre
            ";
            return $this->exec($ClienteSql, 'assoc');
        }
        else{
            return false;
        }
    }
    
    /**
     * 
     * @param string $Nombre
     * @param int $Id
     * @return bool
     */
    public function edita($Nombre, $Id) {

        if($Nombre != "" and $Id > 0){

            $ClienteSql = "
            UPDATE ".ESQUEMA."cliente
                SET nombre = '".  strtoupper(trim($Nombre))."'
            WHERE id_cliente = ".$Id."
            ";
            $this->exec($ClienteSql);
            return true;
        }
        else{
            return false;
        }
    }
}
