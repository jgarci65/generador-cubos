<?php
include_once RUTA_PDO.'PDOSql.php';
/**
 * Description of EventoJavascript
 *
 * @author javierg.garcia
 */
class EventoJavascript extends PDOSql{
    
    /**
     * 
     * @param string $DB
     */
    public function __construct($DB) {
        
        parent::__construct($DB);
    }
    
    /**
     * 
     * @param int $Id
     * @param string $Nombre
     * @return array
     */
    public function consulta($Id = NULL, $Nombre = NULL) {
        
        $FiltroSql = array();
        if ($Id != NULL) $FiltroSql[] = "id_evento_javascript = ".$Id;
        if ($Nombre != NULL) $FiltroSql[] = "nombre = '".$Nombre."'";
        
        $ConsultaSql = "
        SELECT id_evento_javascript, nombre
        FROM ".ESQUEMA."evento_javascript
        ".((count($FiltroSql) > 0) ? "WHERE ".implode(" AND ", $FiltroSql) : "")."
        ";
        return $this->pasarelaSql($ConsultaSql, 'assoc');
    }
    
    /**
     * 
     * @param int $Id
     * @return array
     */
    public function consultaEventosTabla($Id) {
        
        $ConsultaSql = "
        SELECT campo_query AS campo, descripcion_usuario AS descripcion, evento, tre.id_evento_javascript AS id_tipo, icono, ej.nombre AS tipo
        FROM ".ESQUEMA."tablarecurrente_evento tre
            INNER JOIN ".ESQUEMA."evento_javascript ej ON tre.id_evento_javascript = ej.id_evento_javascript
        WHERE id_tablarecurrente = ".$Id."
        ";
        return $this->pasarelaSql($ConsultaSql, 'assoc');
    }
}
