<?php
/**
 * Description of PDOSql
 *
 * @author javierg.garcia
 */
class PDOSql {
    
    private $AutoCommit;
    private $GBD;
    private $Debug = false;
    
    /**
     * 
     * @param string $DB
     * @param boolean $AutoCommit
     */
    public function __construct($DB, $AutoCommit = true) {
        
        $this->AutoCommit = $AutoCommit;
        $Datos = $this->seleccionarDB($DB);
        try {
            $DNS = $Datos["Motor"].":dbname=".$Datos["NombreDB"].";host=".$Datos["Servidor"].(($Datos["Puerto"] != "") ? ";port=".$Datos["Puerto"] : "");
            $this->GBD = new PDO($DNS, $Datos["Usuario"], $Datos["Clave"]);
        } 
        catch (PDOException $e) {
            echo "Falló la conexión: " . $e->getMessage();
            exit;
        }
        $this->setAutoCommit($AutoCommit);
    }
    
    /**
     * 
     * @return boolean
     */
    public function getDebug() {
        return $this->Debug;
    }
    
    /**
     * 
     * @param boolean $Debug
     */
    public function setDebug($Debug) {
        $this->Debug = $Debug;
    }

    /**
     * @deprecated El metodo no funciona adecuadamente, al activar las transacciones se congela el apache
     * 
     * @param boolean $AutoCommit
     */
    public function setAutoCommit($AutoCommit) {
        
        $this->AutoCommit = $AutoCommit;
        # En caso de estar en falso el auto commit, se asigna el inicio de una transacción que debe ser confirmable al final del proceso
        if($this->AutoCommit == false){
            $this->GBD->beginTransaction();
        }
    }
    
    /**
     * 
     * @param string $DB
     * @return array
     */
    private function seleccionarDB($DB) {

        if(is_array($DB)){
            $BasesDeDatos = array(
                $DB['base_de_datos'] => array(
                    'Servidor' => $DB['host'],
                    'NombreDB' => $DB['base_de_datos'],
                    'Usuario' => $DB['usuario'],
                    'Clave' => $DB['clave'],
                    'Motor' => $DB['driver'],
                    'Puerto' => $DB['puerto']
                )
            );
            $DB = $DB['base_de_datos'];
        }
        else{
            $BasesDeDatos = array(
                'generador' => array(
                    'Servidor' => 'localhost',
                    'NombreDB' => 'generador',
                    'Usuario' => 'postgres',
                    'Clave' => 'root',
                    'Motor' => 'pgsql',
                    'Puerto' => '5433'
                ),
            );
        }
        return $BasesDeDatos[$DB];
    }
    
    /**
     * Compatibilidad versión actual
     * 
     * @param string $Sql
     * @param string $Metodo
     * @return array
     */
    public function pasarelaSql($Sql, $Metodo = "assoc") {
        
        return $this->selectSimple($Sql, $Metodo);
    }

    
    protected function numAssoc($Metodo = "assoc"){

        if($Metodo == "both"){
            $fetch_style = PDO::FETCH_BOTH;
        }
        else if($Metodo == "num"){
            $fetch_style = PDO::FETCH_NUM;
        }
        else{
            $fetch_style = PDO::FETCH_ASSOC;
        }
        return $fetch_style;
    }

    /**
     * Documentación: http://php.net/manual/es/pdo.query.php
     * 
     * @param string $Sql
     * @return array
     */
    protected function selectSimple($Sql, $Metodo = "") {
        
        $GSent = $this->GBD->prepare($Sql);
        $Respuesta = $GSent->execute();
        if($this->getDebug() == true){
            echo "<pre>$Sql</pre>";
        }
        if($Respuesta == false){

            echo "Falló la consulta: <br />Descripción error: ".implode("<br />", $GSent->errorInfo( ));
            echo "<pre>$Sql</pre>";
            $this->reversar();
            exit;
        }

        $fetch_style = $this->numAssoc($Metodo);
        return $GSent->fetchAll($fetch_style);
    }
    
    /**
     * Documentación: http://php.net/manual/es/pdo.prepare.php
     * 
     * @param type $Sql
     * @param type $Filtros
     */
    protected function selectPrepare($Sql, $Filtros, $Metodo = "") {
        
        try{
            $STH = $this->GBD->prepare($Sql);
            $STH->execute($Filtros);
            
            if($this->getDebug() == true){
                echo "<pre>$Sql</pre>";
                $STH->debugDumpParams();
            }
            $fetch_style = $this->numAssoc($Metodo);
            return $STH->fetchAll($fetch_style);
        } 
        catch (Exception $e) {
            echo "Falló la consulta: " . $e->getMessage()."<br />Codigo error: ".$this->GBD->errorCode()."<br />Descripción error: ".$this->GBD->errorInfo();
            echo "<pre>$Sql</pre>";
            $this->reversar();
            exit;
        }
    }
    
    /**
     * 
     * @param array $ArPares
     * @param string $Tipo
     * @return string
     */
    protected function crearPar($ArPares, $Tipo) {
        
        $Pares = array();
        $Bind = array();
        foreach ($ArPares as $Campo => $Valor) {
            
            $Pares[] = $Campo." = :".$Tipo.$Campo;
            $Bind[":".$Tipo.$Campo] = $Valor;
        }
        
        return array(
            "Pares" => $Pares,
            "Bind" => $Bind
        );
    }
    
    /**
     * 
     * @param string $Tabla
     * @param array $ArSet
     * @param array $ArWhere
     * @param string $Returning
     * @return array
     */
    protected function update($Tabla, $ArSet, $ArWhere, $Returning = "", $Metodo = "") {
        
        if(count($ArSet) > 0 and count($ArWhere) > 0){
            
            $Set = $this->crearPar($ArSet, "Set");
            $Where = $this->crearPar($ArWhere, "Where");
            
            $Sql = "
            UPDATE ".$Tabla." 
            SET ".implode(",", $Set["Pares"])."
            WHERE ".implode(" AND ", $Where["Pares"])."
            ".(($Returning != "") ? "RETURNING ".$Returning : "")."
            ";
            $GSent = $this->GBD->prepare($Sql);

            if($this->getDebug() == true){
                echo "<pre>$Sql";
                print_r($Set["Bind"]);
                print_r($Where["Bind"]);
                echo "</pre>";
            }
            $Respuesta = $GSent->execute( array_merge($Set["Bind"], $Where["Bind"]) );
            
            if($Respuesta == false){

                echo "Falló la consulta: <br />Descripción error: ".implode("<br />", $GSent->errorInfo( ))."<br />";
                echo "<pre>$Sql</pre>";
                $this->reversar();
                exit;
            }
            
            if($Returning == ""){
                return array();
            }
            else{
                $fetch_style = $this->numAssoc($Metodo);
                return $GSent->fetchAll($fetch_style);
            }
        }
    }
    
    /**
     * 
     * @param string $Tabla
     * @param array $ArWhere
     */
    protected function delete($Tabla, $ArWhere) {
        
        if(count($ArWhere) > 0){
            
            $Where = $this->crearPar($ArWhere, "Where");
            
            $Sql = "DELETE FROM ".$Tabla." WHERE ".  implode(" AND ", $Where["Pares"]);
            $GSent = $this->GBD->prepare($Sql);
            
            if($this->getDebug() == true){
                echo "<pre>$Sql";
                print_r($Where["Bind"]);
                echo "</pre>";
            }
            $Respuesta = $GSent->execute( $Where["Bind"] );
            if($Respuesta == false){

                echo "Falló la consulta: <br />Descripción error: ".implode("<br />", $GSent->errorInfo( ));
                echo "<pre>$Sql</pre>";
                $this->reversar();
                exit;
            }
        }
    }
    
    /**
     * 
     * @param string $Tabla
     * @param array $ArSet
     * @param string $Returning
     * @return array
     */
    protected function insert($Tabla, $ArSet, $Returning = "", $Metodo = "") {
        
        if(count($ArSet) > 0){
            
            $AInsertar = array();
            $Campos = array();
            foreach ($ArSet AS $Campo => $Valor){
                # Tiene un Sub-Query o una funcion
                $Campos[] = ":Insert".$Campo;
                $AInsertar[":Insert".$Campo] = $Valor;
            }
            
            $Sql = "
            INSERT INTO ".$Tabla." (
                ".  implode(", ", array_keys($ArSet))."
            )
            VALUES (
                ".  implode(", ", $Campos)."
            )
            ".(($Returning != "") ? "RETURNING ".$Returning : "")."
            ";
            $GSent = $this->GBD->prepare($Sql);
            
            if($this->getDebug() == true){
                echo "<pre>$Sql";
                print_r($AInsertar);
                echo "</pre>";
            }
            $Respuesta = $GSent->execute( $AInsertar );
            if($Respuesta == false){

                echo "Falló la consulta: <br />Descripción error: ".implode("<br />", $GSent->errorInfo( ));
                echo "<pre>$Sql</pre>";
                $this->reversar();
                exit;
            }
            $fetch_style = $this->numAssoc($Metodo);
            return ($Returning == "") ? array() : $GSent->fetchAll($fetch_style);
        }
    }

    /**
     * @param string $Sql
     * @param string $Metodo
     * @return array
     */
    protected function exec($Sql, $Metodo = "") {
        
        $GSent = $this->GBD->prepare($Sql);
        $Respuesta = $GSent->execute();
        if($this->getDebug() == true){
                echo "<pre>$Sql</pre>";
                print_r($Respuesta);
            }
        if($Respuesta == false){

            echo "Falló la consulta: <br />Descripción error: ".implode("<br />", $GSent->errorInfo( ));
            echo "<pre>$Sql</pre>";
            $this->reversar();
            exit;
        }
        $fetch_style = $this->numAssoc($Metodo);
        return $GSent->fetchAll($fetch_style);
    }
    
    /**
     * @deprecated El metodo no funciona adecuadamente, al activar las transacciones se congela el apache
     */
    public function commit() {
        
        if($this->AutoCommit == false and $this->SeHizoTransaccion == "SI"){
            
            $this->GBD->commit();
        }
    }
    
    protected function reversar() {
        
        if($this->AutoCommit == false and $this->SeHizoTransaccion == "SI"){
            $this->GBD->rollBack();
        }
    }
    
    /**
     * 
     * @return array
     */
    static public function controladoresDisponibles() {
        
        return PDO::getAvailableDrivers();
    }
    
    /**
     * 
     * @param string $Secuencia
     * @return int
     */
    public function insertId($Secuencia) {
        
        return $this->GBD->lastInsertId($Secuencia);
    }
}
