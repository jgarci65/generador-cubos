<?php
include_once RUTA_PDO.'PDOSql.php';
/**
 * Description of BaseDeDatos
 *
 * @author javierg.garcia
 */
class BaseDeDatos extends PDOSql{
    
    public function __construct($DB) {
        
        parent::__construct($DB);
    }
    
    /**
     * 
     * @param int $Id
     * @param string $Nombre
     * @param string $RequerirUso SI|NO
     * @return array
     */
    public function consulta($Id = NULL, $Nombre = NULL, $RequerirUso = 'NO') {
        
        $FiltroSql = array();
        if($Id != NULL) $FiltroSql[] = "id_basededatos = ".$Id."";
        if($Nombre != NULL) $FiltroSql[] = "nombre = '".$Nombre."'";
        
        $ConsultaSql = "
        SELECT id_basededatos, nombre, usuario, clave, base_de_datos, esquema, driver, puerto, host
        ".(($RequerirUso == 'SI') ? ",(SELECT COUNT(*) FROM ".ESQUEMA."tablarecurrente str WHERE str.id_basededatos = bdd.id_basededatos) AS uso" : "")."
        FROM ".ESQUEMA."basededatos bdd
        ".((count($FiltroSql) > 0) ? "WHERE ".implode(" AND ", $FiltroSql) : "")."
        ORDER BY nombre ASC
        ";
        return $this->pasarelaSql($ConsultaSql, 'assoc');
    }

    /**
     * @param string $Nombre
     * @param string $Usuario
     * @param string $Clave
     * @param string $BaseDeDatos
     * @param string $Esquema
     * @param string $Host
     * @param string $Driver
     * @param int $Puerto
     *
     * @return array
     */
    public function inserta($Nombre, $Usuario, $Clave, $BaseDeDatos, $Esquema, $Host, $Driver, $Puerto) {

        if($Nombre != "" and $Usuario != "" and $Clave != "" and $BaseDeDatos != "" and $Host != "" and $Driver != ""){
            
            $BaseDatosSql = "
            INSERT INTO ".ESQUEMA."basededatos (
                nombre, 
                usuario,
                clave,
                base_de_datos,
                esquema,
                host,
                driver,
                puerto
            )
            VALUES (
                '".strtoupper(trim($Nombre))."',
                '".trim($Usuario)."',
                '".trim($Clave)."',
                '".trim($BaseDeDatos)."',
                '".trim($Esquema)."',
                '".trim($Host)."',
                '".trim($Driver)."',
                '".trim($Puerto)."'
            )
            RETURNING id_basededatos, nombre
            ";
            echo $BaseDatosSql;
            return $this->exec($BaseDatosSql, 'assoc');
        }
        else{
            return false;
        }
    }

    /**
     * @param $Nombre
     * @param $Usuario
     * @param $Clave
     * @param $BaseDeDatos
     * @param $Esquema
     * @param $Host
     * @param $Driver
     * @param $Puerto
     * @param $Id
     */
    public function edita($Nombre, $Usuario, $Clave, $BaseDeDatos, $Esquema, $Host, $Driver, $Puerto, $Id) {

        if($Id > 0){

            $BaseDatosSql = "
            UPDATE ".ESQUEMA."basededatos
                SET nombre = '".trim(strtoupper($Nombre))."', 
                usuario = '".trim($Usuario)."',
                clave = '".trim($Clave)."',
                base_de_datos = '".trim($BaseDeDatos)."',
                esquema = '".trim($Esquema)."',
                host = '".trim($Host)."',
                driver = '".trim($Driver)."',
                puerto = '".trim($Puerto)."'
            WHERE id_basededatos = ".$Id."
            ";
            $this->exec($BaseDatosSql);

            return true;
        }
        else{
            return false;
        }
    }
    
    /**
     * .0
     * @param int $Id
     */
    public function elimina($Id) {
        
        if($Id > 0){
            
            $BaseDatosSql = "
            DELETE FROM ".ESQUEMA."basededatos
            WHERE id_basededatos = ".$Id."
            ";
            $this->exec($BaseDatosSql);
            return true;
        }
        else{
            return false;
        }
    }
}
