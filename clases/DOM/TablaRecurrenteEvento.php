                                                                                                                           <?php
include_once RUTA_PDO.'PDOSql.php';
/**
 * Description of TablaRecurrenteEvento
 *
 * @author javierg.garcia
 */
class TablaRecurrenteEvento extends PDOSql{
    
    /**
     * 
     * @param string $DB
     */
    public function __construct($DB) {
        
        parent::__construct($DB);
    }
    
    /**
     * 
     * @param int $IdTabla
     * @param string $NombreEnSql
     * @param int $IdTipoEvento
     * @param string $Descripcion
     * @param string $NombreEvento
     * @param string $Icono
     */
    public function insertaOEdita($IdTabla, $NombreEnSql, $IdTipoEvento, $Descripcion, $NombreEvento, $Icono) {
        
        $UpdateSql = "
        UPDATE ".ESQUEMA."tablarecurrente_evento
        SET descripcion_usuario = '".trim(ucfirst(strtolower($Descripcion)))."', evento = '".trim($NombreEvento)."', icono = '".trim($Icono)."'
        WHERE id_tablarecurrente = ".$IdTabla."
            AND campo_query = '".trim(strtolower($NombreEnSql))."'
            AND id_evento_javascript = ".$IdTipoEvento.";";
        $this->exec($UpdateSql);
        
        $InsertSql = "INSERT INTO ".ESQUEMA."tablarecurrente_evento (
            id_tablarecurrente, descripcion_usuario, campo_query, evento, id_evento_javascript, icono
        )
        SELECT ".$IdTabla.", 
            '".trim(ucfirst(strtolower($Descripcion)))."',
            '".trim(strtolower($NombreEnSql))."',
            '".trim($NombreEvento)."',
            ".$IdTipoEvento.",
            '".trim($Icono)."'
        WHERE NOT EXISTS (
            SELECT 1 FROM ".ESQUEMA."tablarecurrente_evento
            WHERE id_tablarecurrente = ".$IdTabla."
                AND campo_query = '".trim(strtolower($NombreEnSql))."'
                AND id_evento_javascript = ".$IdTipoEvento."
        );
        ";
        $this->exec($InsertSql);
    }
    
    /**
     * 
     * @param int $IdTabla
     * @param array $Eventos
     */
    public function eliminaSiNoEstanEn($IdTabla, $Eventos) {
        
        $CampoEvento = array();
        foreach ($Eventos as $E) {
            $CampoEvento[] = $E['campo']."".$E['id_tipo'];
        }
        
        $EliminaSql = "
        DELETE FROM ".ESQUEMA."tablarecurrente_evento
        WHERE id_tablarecurrente = ".$IdTabla."
            AND campo_query||id_evento_javascript NOT IN ('".implode("','", $CampoEvento)."')
        ";
        $this->exec($EliminaSql);
    }
    
    /**
     * 
     * @param int $IdTablaCopia
     * @param int $IdTablaPega
     */
    public function duplica($IdTablaCopia, $IdTablaPega) {
        
        $DuplicaSql = "
        INSERT INTO ".ESQUEMA."tablarecurrente_evento (
            id_tablarecurrente,
            descripcion_usuario,
            campo_query,
            evento,
            id_evento_javascript,
            icono
        )
        SELECT ".$IdTablaPega.", descripcion_usuario, campo_query, evento, id_evento_javascript, icono
        FROM ".ESQUEMA."tablarecurrente_evento
        WHERE id_tablarecurrente = ".$IdTablaCopia."
        ";
        $this->exec($DuplicaSql);
    }
}
